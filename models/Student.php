<?php 

namespace BWB\Framework\mvc\models;
 Class Student 
 { 
 	 protected $formation; 
 	 protected $account; 

	/** Permet d'affecter l'attribut formation 
	* @param formation 
	*/
	public function setFormation($formation) 
	{ 
		$this->formation = $formation; 
	}

	/** Permet de récupérer la valeur à l'attribut formation
	* return $this 
	*/
	public function getFormation() 
	{ 
		return $this->formation; 
 	} 

	/** Permet d'affecter l'attribut account 
	* @param account 
	*/
	public function setAccount($account) 
	{ 
		$this->account = $account; 
	}

	/** Permet de récupérer la valeur à l'attribut account
	* return $this 
	*/
	public function getAccount() 
	{ 
		return $this->account; 
 	} 
 } 
