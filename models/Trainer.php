<?php 

namespace BWB\Framework\mvc\models;
 Class Trainer 
 { 
 	 protected $formation; 
 	 protected $account; 
 	 protected $coordinator; 

	/** Permet d'affecter l'attribut formation 
	* @param formation 
	*/
	public function setFormation($formation) 
	{ 
		$this->formation = $formation; 
	}

	/** Permet de récupérer la valeur à l'attribut formation
	* return $this 
	*/
	public function getFormation() 
	{ 
		return $this->formation; 
 	} 

	/** Permet d'affecter l'attribut account 
	* @param account 
	*/
	public function setAccount($account) 
	{ 
		$this->account = $account; 
	}

	/** Permet de récupérer la valeur à l'attribut account
	* return $this 
	*/
	public function getAccount() 
	{ 
		return $this->account; 
 	} 

	/** Permet d'affecter l'attribut coordinator 
	* @param coordinator 
	*/
	public function setCoordinator($coordinator) 
	{ 
		$this->coordinator = $coordinator; 
	}

	/** Permet de récupérer la valeur à l'attribut coordinator
	* return $this 
	*/
	public function getCoordinator() 
	{ 
		return $this->coordinator; 
 	} 
 } 
