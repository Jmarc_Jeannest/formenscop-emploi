<?php 

namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\Account;

 Class Trainee extends Account
 { 
 	protected $account; 
	protected $skill; // un trainee possède des skills dans la db. Donc on peut s'en servir en propriété
	protected $abilities = []; // un trainee possède des abilities dans la db. Donc on peut s'en servir en propriété

	/** Permet d'affecter l'attribut account 
	* @param account 
	*/
	public function setAccount($account) 
	{ 
		$this->account = $account; 
	}

	/** Permet de récupérer la valeur à l'attribut account
	* return $this 
	*/
	public function getAccount() 
	{ 
		return $this->account; 
 	} 

	/**
	 * Get the value of skill
	 */ 
	public function getSkill()
	{
		return $this->skill;
	}

	/**
	 * Set the value of skill
	 *
	 * @return  self
	 */ 
	public function setSkill($skill)
	{
		$this->skill = $skill;

		return $this;
	}

	/**
	 * Get the value of abilities
	 */ 
	public function getAbilities()
	{
		return $this->abilities;
	}

	/**
	 * Set the value of abilities
	 *
	 * @return  self
	 */ 
	public function setAbilities($abilities)
	{
		$this->abilities = $abilities;

		return $this;
	}
 } 
