<?php 
namespace BWB\Framework\mvc\models;

use JsonSerializable;

 Class Contract implements JsonSerializable
 { 
 	 protected $id; 
 	 protected $title; 

	/** Permet d'affecter l'attribut id 
	* @param id 
	*/
	public function setId($id) 
	{ 
		$this->id = $id; 
	}

	/** Permet de récupérer la valeur à l'attribut id
	* return $this 
	*/
	public function getId() 
	{ 
		return $this->id; 
 	} 

	/** Permet d'affecter l'attribut title 
	* @param title 
	*/
	public function setTitle($title) 
	{ 
		$this->title = $title; 
	}

	/** Permet de récupérer la valeur à l'attribut title
	* return $this 
	*/
	public function getTitle() 
	{ 
		return $this->title; 
	 } 
	 
	public function jsonSerialize()
	{
	 return 
	 [
		'id' => $this->getId(),
		'title' => $this->getTitle()
	 ];
	}

 } 
