<?php
namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\UserInterface;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\models\Address;
use BWB\Framework\mvc\models\DefaultModel;

use JsonSerializable;

/**
 * Description of Account
 *
 * Ajout du lazy mode pour les comptes afin de choisir d'avoir un utilisateur complet ou juste le compte courant.
 * @author loic
 */
class Account implements UserInterface, JsonSerializable
{
    protected $id, $firstname, $lastname, $address, $role, $birthday, $email, $password;

    private $events;

    protected $lazy; // ajout du lazy mode pour eviter les appels sans fin sur les doubles references. 

    private $unReadMessages, $unValidEvents;

    public function __construct($id = null, $lazy = false)
    {
        $this->lazy = $lazy;
        if (!is_null($id)) {
            $this->parse((new DAOAccount())->retrieve($id));
            
             if (!$this->lazy) {
             //    $this->events = (new DAOAccount())->getEvents($id);
             }
            $this->unReadMessages=(new DAOAccount())->getAllByMessage($id);
            $this->unValidEvents=(new DAOAccount())->getAllByEvent($id);
        }
    }

    public function getId()
   {
       return $this->id;
   }

    public function getRoles()
    {
        return $this->role;
    }

    public function setRole($role){
        $this->role = $role;
        return $this;

    }

    public function getUsername()
    {
        return $this->id;
    }

    public function addEvent($event)
    {
        if (is_object($event)) {
            array_push($this->events, $event);
        } else {
            // a l'ajout d'un event ça peux valoir le coup de rajouter la colorisation pour fullcalendar
            array_push($this->events, new Event($event));
        }
    }
    /**
     * La methode permet d'utiliser le tableau associatif passé en argument
     * pour peupler les propriétés de l'objet courant
     * L'idéal est d'utiliser la methode au sein de la classe courante.
     * Néamoins la methode utilise le mutateur si il est disponible sur l'objet courant.
     */
    public function parse($datas)
    {
        foreach ($datas as $key => $value) {
            if (($postion = strpos($key, "_") > 0)) {
                $tab = explode("_", $key);
                foreach ($tab as $i => $val) {
                    if ($i > 0) {
                        $val = ucfirst($val);
                        $key .= $val;
                    } else {
                        $key = $val;
                    }
                }
            }
            if (method_exists($this, "set" . ucfirst($key))) {
                $method = "set" . ucfirst($key);
                $this->$method($value);
            } elseif (property_exists($this, $key)) {
                $this->$key = $value;
            }
            // Prise en charge de l'ajout dans une liste 
            if (method_exists($this, "add" . ucfirst($key))) {
                $method = "add" . ucfirst($key);
                $this->$method($value);
            }
        }
    }

    /**
     * Get the value of firstname
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set the value of firstname
     *
     * @return  self
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get the value of lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set the value of lastname
     *
     * @return  self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of birthday
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set the value of birthday
     *
     * @return  self
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get the value of address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of address
     * prend en compte le fait d'affecter un objet de type Address a l'objet Account.
     *
     * @return  self
     */
    public function setAddress($address)
    {
        if (is_object($address)) {
            $this->address = $address;
        } else {
            $this->address = new Address($address);
        }

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;


        return $this;
    }

    /**
     * Get the value of events
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Set the value of events
     *
     * @return  self
     */
    public function setEvents($events)
    {
        $this->events = $events;

        return $this;
    }
    /**
     * Implémentation de la methode invoquée par la fonction json_encode si on lui donne un objet.
     */
    public function jsonSerialize()
    {
        $datas = array();
        $props = get_object_vars($this);
        foreach ($props as $propName => $value) {
            // je m'assure juste de respecter les implémentations on get que si il y a un accesseur.
            if (method_exists($this, "get" . ucfirst($propName))) {
                $datas[$propName] = $value;
            }
        }
        return $datas;
    }

    /**
     * Get the value of unValidEvents
     */ 
    public function getUnValidEvents()
    {
        return $this->unValidEvents;
    }

    /**
     * Set the value of unValidEvents
     *
     * @return  self
     */ 
    public function setUnValidEvents($unValidEvents)
    {
        $this->unValidEvents = $unValidEvents;

        return $this;
    }

    /**
     * Get the value of unReadMessages
     */ 
    public function getUnReadMessages()
    {
        return $this->unReadMessages;
    }

    /**
     * Set the value of unReadMessages
     *
     * @return  self
     */ 
    public function setUnReadMessages($unReadMessages)
    {
        $this->unReadMessages = $unReadMessages;

        return $this;
    }
}
