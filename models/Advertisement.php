<?php
namespace BWB\Framework\mvc\models;

use JsonSerializable;

 Class Advertisement implements JsonSerializable
 { 
 	protected $id; 
 	protected $emitter; 
 	protected $contract; 
 	protected $title; 
 	protected $description; 
	protected $date;
	protected $skills;
	protected $interest;

	/** Permet d'affecter l'attribut id 
	* @param id 
	*/
	public function setId($id) 
	{ 
		$this->id = $id; 
	}

	/** Permet de récupérer la valeur à l'attribut id
	* return $this 
	*/
	public function getId() 
	{ 
		return $this->id; 
 	} 

	/** Permet d'affecter l'attribut emitter 
	* @param emitter 
	*/
	public function setEmitter($emitter) 
	{ 
		$this->emitter = $emitter; 
	}

	/** Permet de récupérer la valeur à l'attribut emitter
	* return $this 
	*/
	public function getEmitter() 
	{ 
		return $this->emitter; 
 	} 

	/** Permet d'affecter l'attribut contract 
	* @param contract 
	*/
	public function setContract($contract) 
	{ 
		$this->contract = $contract; 
	}

	/** Permet de récupérer la valeur à l'attribut contract
	* return $this 
	*/
	public function getContract() 
	{ 
		return $this->contract; 
 	} 

	/** Permet d'affecter l'attribut title 
	* @param title 
	*/
	public function setTitle($title) 
	{ 
		$this->title = $title; 
	}

	/** Permet de récupérer la valeur à l'attribut title
	* return $this 
	*/
	public function getTitle() 
	{ 
		return $this->title; 
 	} 

	/** Permet d'affecter l'attribut description 
	* @param description 
	*/
	public function setDescription($description) 
	{ 
		$this->description = $description; 
	}

	/** Permet de récupérer la valeur à l'attribut description
	* return $this 
	*/
	public function getDescription() 
	{ 
		return $this->description; 
 	} 

	/** Permet d'affecter l'attribut date 
	* @param date 
	*/
	public function setDate($date) 
	{ 
		$this->date = $date; 
	}

	/** Permet de récupérer la valeur à l'attribut date
	* return $this 
	*/
	public function getDate() 
	{ 
		return $this->date; 
 	} 

	/**
	 * Get the value of skills
	 */ 
	public function getSkills()
	{
		return $this->skills;
	}

	/**
	 * Set the value of skills
	 *
	 * @return  self
	 */ 
	public function setSkills($skills)
	{
		$this->skills = $skills;

		return $this;
	}

	/**
	 * Get the value of interest
	 */ 
	public function getInterest()
	{
		return $this->interest;
	}

	/**
	 * Set the value of interest
	 *
	 * @return  self
	 */ 
	public function setInterest($interest)
	{
		$this->interest = $interest;

		return $this;
	}

	public function jsonSerialize()
    {
        return 
        [
            'id'   => $this->getId(),
 			'emitter' => $this->getEmitter() , 
 			'contract' => $this->getContract() , 
 			'title' => $this->getTitle() , 
 			'description' => $this->getDescription() , 
			'date' => $this->getDate() ,
			'skills' => $this->getSkills() ,
			'interest' => $this->getInterest() 
        ];
    }
 } 
