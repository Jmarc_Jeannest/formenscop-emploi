<?php 
namespace BWB\Framework\mvc\models;
use JsonSerializable;

 Class Account implements JsonSerializable
 { 
 	protected $id; 
 	protected $firstname; 
 	protected $lastname; 
 	protected $address; 
 	protected $role; 
 	protected $birthday; 
 	protected $email; 
	protected $password;
	//protected $state_interest;

	/** Permet d'affecter l'attribut id 
	* @param id 
	*/
	public function setId($id) 
	{ 
		$this->id = $id; 
	}

	/** Permet de récupérer la valeur à l'attribut id
	* return $this 
	*/
	public function getId() 
	{ 
		return $this->id; 
 	} 

	/** Permet d'affecter l'attribut firstname 
	* @param firstname 
	*/
	public function setFirstname($firstname) 
	{ 
		$this->firstname = $firstname; 
	}

	/** Permet de récupérer la valeur à l'attribut firstname
	* return $this 
	*/
	public function getFirstname() 
	{ 
		return $this->firstname; 
 	} 

	/** Permet d'affecter l'attribut lastname 
	* @param lastname 
	*/
	public function setLastname($lastname) 
	{ 
		$this->lastname = $lastname; 
	}

	/** Permet de récupérer la valeur à l'attribut lastname
	* return $this 
	*/
	public function getLastname() 
	{ 
		return $this->lastname; 
 	} 

	/** Permet d'affecter l'attribut address 
	* @param address 
	*/
	public function setAddress($address) 
	{ 
		$this->address = $address; 
	}

	/** Permet de récupérer la valeur à l'attribut address
	* return $this 
	*/
	public function getAddress() 
	{ 
		return $this->address; 
 	} 

	/** Permet d'affecter l'attribut role 
	* @param role 
	*/
	public function setRole($role) 
	{ 
		$this->role = $role; 
	}

	/** Permet de récupérer la valeur à l'attribut role
	* return $this 
	*/
	public function getRole() 
	{ 
		return $this->role; 
 	} 

	/** Permet d'affecter l'attribut birthday 
	* @param birthday 
	*/
	public function setBirthday($birthday) 
	{ 
		$this->birthday = $birthday; 
	}

	/** Permet de récupérer la valeur à l'attribut birthday
	* return $this 
	*/
	public function getBirthday() 
	{ 
		return $this->birthday; 
 	} 

	/** Permet d'affecter l'attribut email 
	* @param email 
	*/
	public function setEmail($email) 
	{ 
		$this->email = $email; 
	}

	/** Permet de récupérer la valeur à l'attribut email
	* return $this 
	*/
	public function getEmail() 
	{ 
		return $this->email; 
 	} 

	/** Permet d'affecter l'attribut password 
	* @param password 
	*/
	public function setPassword($password) 
	{ 
		$this->password = $password; 
	}

	/** Permet de récupérer la valeur à l'attribut password
	* return $this 
	*/
	public function getPassword() 
	{ 
		return $this->password; 
 	} 

	/**
	 * Get the value of state_interest
	 */ 
	public function getState_interest()
	{
		return $this->state_interest;
	}

	/**
	 * Set the value of state_interest
	 *
	 * @return  self
	 */ 
	public function setState_interest($state_interest)
	{
		$this->state_interest = $state_interest;

		return $this;
	}

	public function jsonSerialize()
    {
        return 
        [
			'id'	=> $this->getId() , 
			'firstname'	=> $this->getFirstname() , 
			'lastname'	=> $this->getLastname() , 
			'address'	=> $this->getAddress() , 
			'role'	=> $this->getRole() , 
			'birthday'	=> $this->getBirthday() , 
			'email'	=> $this->getEmail() , 
			'password'	=> $this->getPassword() ,
			'state_interest'	=> $this->getState_interest() 
        ];
    }
 }