<?php 

namespace BWB\Framework\mvc\models;
 Class Message 
 { 
 	 protected $id; 
 	 protected $subject; 
 	 protected $body; 
 	 protected $read; 

	/** Permet d'affecter l'attribut id 
	* @param id 
	*/
	public function setId($id) 
	{ 
		$this->id = $id; 
	}

	/** Permet de récupérer la valeur à l'attribut id
	* return $this 
	*/
	public function getId() 
	{ 
		return $this->id; 
 	} 

	/** Permet d'affecter l'attribut subject 
	* @param subject 
	*/
	public function setSubject($subject) 
	{ 
		$this->subject = $subject; 
	}

	/** Permet de récupérer la valeur à l'attribut subject
	* return $this 
	*/
	public function getSubject() 
	{ 
		return $this->subject; 
 	} 

	/** Permet d'affecter l'attribut body 
	* @param body 
	*/
	public function setBody($body) 
	{ 
		$this->body = $body; 
	}

	/** Permet de récupérer la valeur à l'attribut body
	* return $this 
	*/
	public function getBody() 
	{ 
		return $this->body; 
 	} 

	/** Permet d'affecter l'attribut read 
	* @param read 
	*/
	public function setRead($read) 
	{ 
		$this->read = $read; 
	}

	/** Permet de récupérer la valeur à l'attribut read
	* return $this 
	*/
	public function getRead() 
	{ 
		return $this->read; 
 	} 
 } 
