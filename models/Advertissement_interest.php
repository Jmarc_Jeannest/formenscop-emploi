<?php 

namespace BWB\Framework\mvc\models;
 Class Advertissement_interest 
 { 
 	 protected $advertissement; 
 	 protected $account; 
 	 protected $state; 

	/** Permet d'affecter l'attribut advertissement 
	* @param advertissement 
	*/
	public function setAdvertissement($advertissement) 
	{ 
		$this->advertissement = $advertissement; 
	}

	/** Permet de récupérer la valeur à l'attribut advertissement
	* return $this 
	*/
	public function getAdvertissement() 
	{ 
		return $this->advertissement; 
 	} 

	/** Permet d'affecter l'attribut account 
	* @param account 
	*/
	public function setAccount($account) 
	{ 
		$this->account = $account; 
	}

	/** Permet de récupérer la valeur à l'attribut account
	* return $this 
	*/
	public function getAccount() 
	{ 
		return $this->account; 
 	} 

	/** Permet d'affecter l'attribut state 
	* @param state 
	*/
	public function setState($state) 
	{ 
		$this->state = $state; 
	}

	/** Permet de récupérer la valeur à l'attribut state
	* return $this 
	*/
	public function getState() 
	{ 
		return $this->state; 
 	} 
 } 
