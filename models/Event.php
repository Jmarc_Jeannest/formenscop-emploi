<?php 

namespace BWB\Framework\mvc\models;
 Class Event 
 { 
 	 protected $id; 
 	 protected $title; 
 	 protected $description; 
 	 protected $db; 
 	 protected $name; 
 	 protected $body; 
 	 protected $definer; 
 	 protected $executeAt; 
 	 protected $intervalValue; 
 	 protected $intervalField; 
 	 protected $created; 
 	 protected $modified; 
 	 protected $lastExecuted; 
 	 protected $starts; 
 	 protected $ends; 
 	 protected $status; 
 	 protected $onCompletion; 
 	 protected $sqlMode; 
 	 protected $comment; 
 	 protected $originator; 
 	 protected $timeZone; 
 	 protected $characterSetClient; 
 	 protected $collationConnection; 
 	 protected $dbCollation; 
 	 protected $bodyUtf8; 

	/** Permet d'affecter l'attribut id 
	* @param id 
	*/
	public function setId($id) 
	{ 
		$this->id = $id; 
	}

	/** Permet de récupérer la valeur à l'attribut id
	* return $this 
	*/
	public function getId() 
	{ 
		return $this->id; 
 	} 

	/** Permet d'affecter l'attribut title 
	* @param title 
	*/
	public function setTitle($title) 
	{ 
		$this->title = $title; 
	}

	/** Permet de récupérer la valeur à l'attribut title
	* return $this 
	*/
	public function getTitle() 
	{ 
		return $this->title; 
 	} 

	/** Permet d'affecter l'attribut description 
	* @param description 
	*/
	public function setDescription($description) 
	{ 
		$this->description = $description; 
	}

	/** Permet de récupérer la valeur à l'attribut description
	* return $this 
	*/
	public function getDescription() 
	{ 
		return $this->description; 
 	} 

	/** Permet d'affecter l'attribut db 
	* @param db 
	*/
	public function setDb($db) 
	{ 
		$this->db = $db; 
	}

	/** Permet de récupérer la valeur à l'attribut db
	* return $this 
	*/
	public function getDb() 
	{ 
		return $this->db; 
 	} 

	/** Permet d'affecter l'attribut name 
	* @param name 
	*/
	public function setName($name) 
	{ 
		$this->name = $name; 
	}

	/** Permet de récupérer la valeur à l'attribut name
	* return $this 
	*/
	public function getName() 
	{ 
		return $this->name; 
 	} 

	/** Permet d'affecter l'attribut body 
	* @param body 
	*/
	public function setBody($body) 
	{ 
		$this->body = $body; 
	}

	/** Permet de récupérer la valeur à l'attribut body
	* return $this 
	*/
	public function getBody() 
	{ 
		return $this->body; 
 	} 

	/** Permet d'affecter l'attribut definer 
	* @param definer 
	*/
	public function setDefiner($definer) 
	{ 
		$this->definer = $definer; 
	}

	/** Permet de récupérer la valeur à l'attribut definer
	* return $this 
	*/
	public function getDefiner() 
	{ 
		return $this->definer; 
 	} 

	/** Permet d'affecter l'attribut executeAt 
	* @param executeAt 
	*/
	public function setExecuteAt($executeAt) 
	{ 
		$this->executeAt = $executeAt; 
	}

	/** Permet de récupérer la valeur à l'attribut executeAt
	* return $this 
	*/
	public function getExecuteAt() 
	{ 
		return $this->executeAt; 
 	} 

	/** Permet d'affecter l'attribut intervalValue 
	* @param intervalValue 
	*/
	public function setIntervalValue($intervalValue) 
	{ 
		$this->intervalValue = $intervalValue; 
	}

	/** Permet de récupérer la valeur à l'attribut intervalValue
	* return $this 
	*/
	public function getIntervalValue() 
	{ 
		return $this->intervalValue; 
 	} 

	/** Permet d'affecter l'attribut intervalField 
	* @param intervalField 
	*/
	public function setIntervalField($intervalField) 
	{ 
		$this->intervalField = $intervalField; 
	}

	/** Permet de récupérer la valeur à l'attribut intervalField
	* return $this 
	*/
	public function getIntervalField() 
	{ 
		return $this->intervalField; 
 	} 

	/** Permet d'affecter l'attribut created 
	* @param created 
	*/
	public function setCreated($created) 
	{ 
		$this->created = $created; 
	}

	/** Permet de récupérer la valeur à l'attribut created
	* return $this 
	*/
	public function getCreated() 
	{ 
		return $this->created; 
 	} 

	/** Permet d'affecter l'attribut modified 
	* @param modified 
	*/
	public function setModified($modified) 
	{ 
		$this->modified = $modified; 
	}

	/** Permet de récupérer la valeur à l'attribut modified
	* return $this 
	*/
	public function getModified() 
	{ 
		return $this->modified; 
 	} 

	/** Permet d'affecter l'attribut lastExecuted 
	* @param lastExecuted 
	*/
	public function setLastExecuted($lastExecuted) 
	{ 
		$this->lastExecuted = $lastExecuted; 
	}

	/** Permet de récupérer la valeur à l'attribut lastExecuted
	* return $this 
	*/
	public function getLastExecuted() 
	{ 
		return $this->lastExecuted; 
 	} 

	/** Permet d'affecter l'attribut starts 
	* @param starts 
	*/
	public function setStarts($starts) 
	{ 
		$this->starts = $starts; 
	}

	/** Permet de récupérer la valeur à l'attribut starts
	* return $this 
	*/
	public function getStarts() 
	{ 
		return $this->starts; 
 	} 

	/** Permet d'affecter l'attribut ends 
	* @param ends 
	*/
	public function setEnds($ends) 
	{ 
		$this->ends = $ends; 
	}

	/** Permet de récupérer la valeur à l'attribut ends
	* return $this 
	*/
	public function getEnds() 
	{ 
		return $this->ends; 
 	} 

	/** Permet d'affecter l'attribut status 
	* @param status 
	*/
	public function setStatus($status) 
	{ 
		$this->status = $status; 
	}

	/** Permet de récupérer la valeur à l'attribut status
	* return $this 
	*/
	public function getStatus() 
	{ 
		return $this->status; 
 	} 

	/** Permet d'affecter l'attribut onCompletion 
	* @param onCompletion 
	*/
	public function setOnCompletion($onCompletion) 
	{ 
		$this->onCompletion = $onCompletion; 
	}

	/** Permet de récupérer la valeur à l'attribut onCompletion
	* return $this 
	*/
	public function getOnCompletion() 
	{ 
		return $this->onCompletion; 
 	} 

	/** Permet d'affecter l'attribut sqlMode 
	* @param sqlMode 
	*/
	public function setSqlMode($sqlMode) 
	{ 
		$this->sqlMode = $sqlMode; 
	}

	/** Permet de récupérer la valeur à l'attribut sqlMode
	* return $this 
	*/
	public function getSqlMode() 
	{ 
		return $this->sqlMode; 
 	} 

	/** Permet d'affecter l'attribut comment 
	* @param comment 
	*/
	public function setComment($comment) 
	{ 
		$this->comment = $comment; 
	}

	/** Permet de récupérer la valeur à l'attribut comment
	* return $this 
	*/
	public function getComment() 
	{ 
		return $this->comment; 
 	} 

	/** Permet d'affecter l'attribut originator 
	* @param originator 
	*/
	public function setOriginator($originator) 
	{ 
		$this->originator = $originator; 
	}

	/** Permet de récupérer la valeur à l'attribut originator
	* return $this 
	*/
	public function getOriginator() 
	{ 
		return $this->originator; 
 	} 

	/** Permet d'affecter l'attribut timeZone 
	* @param timeZone 
	*/
	public function setTimeZone($timeZone) 
	{ 
		$this->timeZone = $timeZone; 
	}

	/** Permet de récupérer la valeur à l'attribut timeZone
	* return $this 
	*/
	public function getTimeZone() 
	{ 
		return $this->timeZone; 
 	} 

	/** Permet d'affecter l'attribut characterSetClient 
	* @param characterSetClient 
	*/
	public function setCharacterSetClient($characterSetClient) 
	{ 
		$this->characterSetClient = $characterSetClient; 
	}

	/** Permet de récupérer la valeur à l'attribut characterSetClient
	* return $this 
	*/
	public function getCharacterSetClient() 
	{ 
		return $this->characterSetClient; 
 	} 

	/** Permet d'affecter l'attribut collationConnection 
	* @param collationConnection 
	*/
	public function setCollationConnection($collationConnection) 
	{ 
		$this->collationConnection = $collationConnection; 
	}

	/** Permet de récupérer la valeur à l'attribut collationConnection
	* return $this 
	*/
	public function getCollationConnection() 
	{ 
		return $this->collationConnection; 
 	} 

	/** Permet d'affecter l'attribut dbCollation 
	* @param dbCollation 
	*/
	public function setDbCollation($dbCollation) 
	{ 
		$this->dbCollation = $dbCollation; 
	}

	/** Permet de récupérer la valeur à l'attribut dbCollation
	* return $this 
	*/
	public function getDbCollation() 
	{ 
		return $this->dbCollation; 
 	} 

	/** Permet d'affecter l'attribut bodyUtf8 
	* @param bodyUtf8 
	*/
	public function setBodyUtf8($bodyUtf8) 
	{ 
		$this->bodyUtf8 = $bodyUtf8; 
	}

	/** Permet de récupérer la valeur à l'attribut bodyUtf8
	* return $this 
	*/
	public function getBodyUtf8() 
	{ 
		return $this->bodyUtf8; 
 	} 
 } 
