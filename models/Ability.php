<?php 

namespace BWB\Framework\mvc\models;
 Class Ability 
 { 
 	 protected $id; 
 	 protected $trainee; 
 	 protected $skill; 

	/** Permet d'affecter l'attribut id 
	* @param id 
	*/
	public function setId($id) 
	{ 
		$this->id = $id; 
	}

	/** Permet de récupérer la valeur à l'attribut id
	* return $this 
	*/
	public function getId() 
	{ 
		return $this->id; 
 	} 

	/** Permet d'affecter l'attribut trainee 
	* @param trainee 
	*/
	public function setTrainee($trainee) 
	{ 
		$this->trainee = $trainee; 
	}

	/** Permet de récupérer la valeur à l'attribut trainee
	* return $this 
	*/
	public function getTrainee() 
	{ 
		return $this->trainee; 
 	} 

	/** Permet d'affecter l'attribut skill 
	* @param skill 
	*/
	public function setSkill($skill) 
	{ 
		$this->skill = $skill; 
	}

	/** Permet de récupérer la valeur à l'attribut skill
	* return $this 
	*/
	public function getSkill() 
	{ 
		return $this->skill; 
 	} 
 } 
