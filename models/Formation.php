<?php 

namespace BWB\Framework\mvc\models;
 Class Formation 
 { 
 	 protected $id; 
 	 protected $title; 
 	 protected $description; 
 	 protected $start; 
 	 protected $end; 
 	 protected $volume; 

	/** Permet d'affecter l'attribut id 
	* @param id 
	*/
	public function setId($id) 
	{ 
		$this->id = $id; 
	}

	/** Permet de récupérer la valeur à l'attribut id
	* return $this 
	*/
	public function getId() 
	{ 
		return $this->id; 
 	} 

	/** Permet d'affecter l'attribut title 
	* @param title 
	*/
	public function setTitle($title) 
	{ 
		$this->title = $title; 
	}

	/** Permet de récupérer la valeur à l'attribut title
	* return $this 
	*/
	public function getTitle() 
	{ 
		return $this->title; 
 	} 

	/** Permet d'affecter l'attribut description 
	* @param description 
	*/
	public function setDescription($description) 
	{ 
		$this->description = $description; 
	}

	/** Permet de récupérer la valeur à l'attribut description
	* return $this 
	*/
	public function getDescription() 
	{ 
		return $this->description; 
 	} 

	/** Permet d'affecter l'attribut start 
	* @param start 
	*/
	public function setStart($start) 
	{ 
		$this->start = $start; 
	}

	/** Permet de récupérer la valeur à l'attribut start
	* return $this 
	*/
	public function getStart() 
	{ 
		return $this->start; 
 	} 

	/** Permet d'affecter l'attribut end 
	* @param end 
	*/
	public function setEnd($end) 
	{ 
		$this->end = $end; 
	}

	/** Permet de récupérer la valeur à l'attribut end
	* return $this 
	*/
	public function getEnd() 
	{ 
		return $this->end; 
 	} 

	/** Permet d'affecter l'attribut volume 
	* @param volume 
	*/
	public function setVolume($volume) 
	{ 
		$this->volume = $volume; 
	}

	/** Permet de récupérer la valeur à l'attribut volume
	* return $this 
	*/
	public function getVolume() 
	{ 
		return $this->volume; 
 	} 
 } 
