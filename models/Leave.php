<?php 

namespace BWB\Framework\mvc\models;
 Class Leave 
 { 
 	 protected $id; 
 	 protected $account; 
 	 protected $start; 
 	 protected $end; 
 	 protected $valid; 

	/** Permet d'affecter l'attribut id 
	* @param id 
	*/
	public function setId($id) 
	{ 
		$this->id = $id; 
	}

	/** Permet de récupérer la valeur à l'attribut id
	* return $this 
	*/
	public function getId() 
	{ 
		return $this->id; 
 	} 

	/** Permet d'affecter l'attribut account 
	* @param account 
	*/
	public function setAccount($account) 
	{ 
		$this->account = $account; 
	}

	/** Permet de récupérer la valeur à l'attribut account
	* return $this 
	*/
	public function getAccount() 
	{ 
		return $this->account; 
 	} 

	/** Permet d'affecter l'attribut start 
	* @param start 
	*/
	public function setStart($start) 
	{ 
		$this->start = $start; 
	}

	/** Permet de récupérer la valeur à l'attribut start
	* return $this 
	*/
	public function getStart() 
	{ 
		return $this->start; 
 	} 

	/** Permet d'affecter l'attribut end 
	* @param end 
	*/
	public function setEnd($end) 
	{ 
		$this->end = $end; 
	}

	/** Permet de récupérer la valeur à l'attribut end
	* return $this 
	*/
	public function getEnd() 
	{ 
		return $this->end; 
 	} 

	/** Permet d'affecter l'attribut valid 
	* @param valid 
	*/
	public function setValid($valid) 
	{ 
		$this->valid = $valid; 
	}

	/** Permet de récupérer la valeur à l'attribut valid
	* return $this 
	*/
	public function getValid() 
	{ 
		return $this->valid; 
 	} 
 } 
