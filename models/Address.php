<?php
namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\Type;
use BWB\Framework\mvc\dao\DAOAddress;

/**
 * Correspond à la table address
 * La proriété $type est un objet de type Type le mutateur prend donc en compte cette spécificité.
 */
class Address
{
    private $id, $street, $number, $type, $additional, $zipCode, $city, $country;

    public function __construct($id = null)
    {
        if (!is_null($id)) {
            $this->parse((new DAOAddress())->retrieve($id));
        }
    }


    /**
     * La methode permet d'utiliser le tableau associatif passé en argument
     * pour peupler les propriétés de l'objet courant
     * L'idéal est d'utiliser la methode au sein de la classe courante.
     * Néamoins la methode utilise le mutateur si il est disponible sur l'objet courant.
     * prend aussi en charge la conversion (_ vers camelCase)
     */
    public function parse($datas)
    {
        foreach ($datas as $key => $value) {
            if (($postion = strpos($key, "_") > 0)) {
                $tab = explode("_", $key);
                foreach ($tab as $i => $val) {
                    if ($i > 0) {
                        $val = ucfirst($val);
                        $key .= $val;
                    } else {
                        $key = $val;
                    }
                }
            }
            if (method_exists($this, "set" . ucfirst($key))) {
                $method = "set" . ucfirst($key);
                $this->$method($value);
            } elseif (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }
    /**
     * Get the value of country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of country
     *
     * @return  self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the value of city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @return  self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of zipCode
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set the value of zipCode
     *
     * @return  self
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get the value of additional
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * Set the value of additional
     *
     * 
     * @return  self
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;

        return $this;
    }

    /**
     * Get the value of type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     * Prend en argument l'information permettant d'affecter le bon type de rue.
     * La propriété type DOIT être un objet de type TYPE. 
     *
     * @param mixed $type Le type de rue de l'adresse. soit on passe un objet de type Type soit l'id du type.
     * @return  self
     */
    public function setType($type)
    {
        if (is_object($type)) {
            $this->type = $type;
        } else {
            $this->type = new Type($type);
        }


        return $this;
    }

    /**
     * Get the value of number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set the value of number
     *
     * @return  self
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get the value of street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set the value of street
     *
     * @return  self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}

