<?php 
namespace BWB\Framework\mvc\models;
use JsonSerializable;

 Class Skill implements JsonSerializable
 { 
 	 protected $id; 
 	 protected $designation; 
 	 protected $description; 

	/** Permet d'affecter l'attribut id 
	* @param id 
	*/
	public function setId($id) 
	{ 
		$this->id = $id; 
	}

	/** Permet de récupérer la valeur à l'attribut id
	* return $this 
	*/
	public function getId() 
	{ 
		return $this->id; 
 	} 

	/** Permet d'affecter l'attribut designation 
	* @param designation 
	*/
	public function setDesignation($designation) 
	{ 
		$this->designation = $designation; 
	}

	/** Permet de récupérer la valeur à l'attribut designation
	* return $this 
	*/
	public function getDesignation() 
	{ 
		return $this->designation; 
 	} 

	/** Permet d'affecter l'attribut description 
	* @param description 
	*/
	public function setDescription($description) 
	{ 
		$this->description = $description; 
	}

	/** Permet de récupérer la valeur à l'attribut description
	* return $this 
	*/
	public function getDescription() 
	{ 
		return $this->description; 
	} 
	 
	public function jsonSerialize()
	{
	 return 
	 [
		'id' => $this->getId(), 
		'designation' => $this->getDesignation(), 
		'description' => $this->getDescription()
	 ];
	}
 } 
