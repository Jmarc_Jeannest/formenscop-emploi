<?php 

namespace BWB\Framework\mvc\models;
 Class Communication 
 { 
 	 protected $message; 
 	 protected $sender; 
 	 protected $receiver; 

	/** Permet d'affecter l'attribut message 
	* @param message 
	*/
	public function setMessage($message) 
	{ 
		$this->message = $message; 
	}

	/** Permet de récupérer la valeur à l'attribut message
	* return $this 
	*/
	public function getMessage() 
	{ 
		return $this->message; 
 	} 

	/** Permet d'affecter l'attribut sender 
	* @param sender 
	*/
	public function setSender($sender) 
	{ 
		$this->sender = $sender; 
	}

	/** Permet de récupérer la valeur à l'attribut sender
	* return $this 
	*/
	public function getSender() 
	{ 
		return $this->sender; 
 	} 

	/** Permet d'affecter l'attribut receiver 
	* @param receiver 
	*/
	public function setReceiver($receiver) 
	{ 
		$this->receiver = $receiver; 
	}

	/** Permet de récupérer la valeur à l'attribut receiver
	* return $this 
	*/
	public function getReceiver() 
	{ 
		return $this->receiver; 
 	} 
 } 
