<?php 
namespace BWB\Framework\mvc\models;


 Class Company extends Account

 { 
 	 protected $account; 
 	 protected $coach; 

	/** Permet d'affecter l'attribut account 
	* @param account 
	*/
	public function setAccount($account) 
	{ 
		$this->account = $account; 
	}

	/** Permet de récupérer la valeur à l'attribut account
	* return $this 
	*/
	public function getAccount() 
	{ 
		return $this->account; 
 	} 

	/** Permet d'affecter l'attribut coach 
	* @param coach 
	*/
	public function setCoach($coach) 
	{ 
		$this->coach = $coach; 
	}

	/** Permet de récupérer la valeur à l'attribut coach
	* return $this 
	*/
	public function getCoach() 
	{ 
		return $this->coach; 
	 } 
	 
	 public function jsonSerialize()
	 {
		 return 
		 [
			'account' => $this->getAccount() ,
			'coach' => $this->getCoach() 
		 ];
	 }


 } 
