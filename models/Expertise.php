<?php 

namespace BWB\Framework\mvc\models;
 Class Expertise 
 { 
 	 protected $advertissement; 
 	 protected $skill; 

	/** Permet d'affecter l'attribut advertissement 
	* @param advertissement 
	*/
	public function setAdvertissement($advertissement) 
	{ 
		$this->advertissement = $advertissement; 
	}

	/** Permet de récupérer la valeur à l'attribut advertissement
	* return $this 
	*/
	public function getAdvertissement() 
	{ 
		return $this->advertissement; 
 	} 

	/** Permet d'affecter l'attribut skill 
	* @param skill 
	*/
	public function setSkill($skill) 
	{ 
		$this->skill = $skill; 
	}

	/** Permet de récupérer la valeur à l'attribut skill
	* return $this 
	*/
	public function getSkill() 
	{ 
		return $this->skill; 
 	} 
 } 
