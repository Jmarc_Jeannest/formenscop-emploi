<?php 

namespace BWB\Framework\mvc\models;
 Class Attendance 
 { 
 	 protected $student; 
 	 protected $date; 
 	 protected $am; 
 	 protected $pm; 
 	 protected $note; 

	/** Permet d'affecter l'attribut student 
	* @param student 
	*/
	public function setStudent($student) 
	{ 
		$this->student = $student; 
	}

	/** Permet de récupérer la valeur à l'attribut student
	* return $this 
	*/
	public function getStudent() 
	{ 
		return $this->student; 
 	} 

	/** Permet d'affecter l'attribut date 
	* @param date 
	*/
	public function setDate($date) 
	{ 
		$this->date = $date; 
	}

	/** Permet de récupérer la valeur à l'attribut date
	* return $this 
	*/
	public function getDate() 
	{ 
		return $this->date; 
 	} 

	/** Permet d'affecter l'attribut am 
	* @param am 
	*/
	public function setAm($am) 
	{ 
		$this->am = $am; 
	}

	/** Permet de récupérer la valeur à l'attribut am
	* return $this 
	*/
	public function getAm() 
	{ 
		return $this->am; 
 	} 

	/** Permet d'affecter l'attribut pm 
	* @param pm 
	*/
	public function setPm($pm) 
	{ 
		$this->pm = $pm; 
	}

	/** Permet de récupérer la valeur à l'attribut pm
	* return $this 
	*/
	public function getPm() 
	{ 
		return $this->pm; 
 	} 

	/** Permet d'affecter l'attribut note 
	* @param note 
	*/
	public function setNote($note) 
	{ 
		$this->note = $note; 
	}

	/** Permet de récupérer la valeur à l'attribut note
	* return $this 
	*/
	public function getNote() 
	{ 
		return $this->note; 
 	} 
 } 
