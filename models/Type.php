<?php
namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\dao\DAOType;

class Type
{
    private $id, $designation;

    public function __construct($id = null)
    {
        if (!is_null($id)) {
            $this->parse((new DAOType())->retrieve($id));
        }
    }


    /**
     * La methode permet d'utiliser le tableau associatif passé en argument
     * pour peupler les propriétés de l'objet courant
     * L'idéal est d'utiliser la methode au sein de la classe courante.
     * Néamoins la methode utilise le mutateur si il est disponible sur l'objet courant.
     * prend aussi en charge la conversion (_ vers camelCase)
     */
    public function parse($datas)
    {
        foreach ($datas as $key => $value) {
            if (($postion = strpos($key, "_") > 0)) {
                $tab = explode("_", $key);
                foreach ($tab as $i => $val) {
                    if ($i > 0) {
                        $val = ucfirst($val);
                        $key .= $val;
                    } else {
                        $key = $val;
                    }
                }
            }
            if (method_exists($this, "set" . ucfirst($key))) {
                $method = "set" . ucfirst($key);
                $this->$method($value);
            } elseif (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of designation
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set the value of designation
     *
     * @return  self
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }
}
