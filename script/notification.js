$(document).ready(function(){
 getUnReadMessages();
 getUnValidEvents();
})
function getUnReadMessages(){
  $.ajax({
    type:"GET",
    url:"/unreadmessages",
    success:function(response){
      $("#messages").text(response);
     
      setTimeout(function(){
        getUnReadMessages()
      },10000)
    },
    error : function(error){
      console.log(error.responseText);
    }


  });
}

function getUnValidEvents(){
  $.ajax({
    type:"GET",
    url:"/unvalidevents",
    success:function(response){
      $("#events").text(response);

      setTimeout(function(){
        getUnValidEvents()
      },10000)
    },
    error : function(error){
      console.log(error.responseText);
    }


  });
}