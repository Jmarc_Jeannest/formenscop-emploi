# FORMENSCOP EMPLOI
---

## Pré-requis logiciels
- Composer : https://getcomposer.org/doc/00-intro.md
- PHP 7.2 : http://php.net/manual/fr/install.php
- Apache 2 : https://httpd.apache.org/download.cgi
- MySql 5.7 : https://dev.mysql.com/doc/refman/5.7/en/mysql-installer-setup.html
- GIT : https://git-scm.com/downloads

## Installation
- Clônage du dépôt : [Formenscop Emploi](https://gitlab.com/beweb-lunel-04/internal-projects/formenscop-emploi.git)
- Exécuter la commande install de composer `composer install` si composer est installé en global ou `php composer.phar install`
- Vérifier que le monde rewrite est bien activé dans Apache2. Il est nécessaire pour le bon fonctionnement du fichier .htaccess
- Mettre en place un virtual host avec pour obligation, le nom de domaine `ServerName emploi.formenscop.bwb`
- Importer le fichier formenscop.sql : 
    - Pour Windows : `mysql -u[utilisateur] -p [nom_base_de_donnees] < formenscop.sql`
    - Pour Linux : `mysql nom_base_de_donnees < formenscop.sql`
-  A la racine de l'application, modifier le fichier database.json avec les informations relatives à votre serveur MySql. 

## Test

- se connecter avec le compte :

    - login : rquilterf@geocities.com
    - mot de passe : IBM

