-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: formenscop-db
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ability`
--

DROP TABLE IF EXISTS `ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trainee` int(11) NOT NULL,
  `skill` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trainee` (`trainee`),
  KEY `skill` (`skill`),
  CONSTRAINT `ability_ibfk_1` FOREIGN KEY (`trainee`) REFERENCES `trainee` (`account`),
  CONSTRAINT `ability_ibfk_2` FOREIGN KEY (`skill`) REFERENCES `skill` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ability`
--

LOCK TABLES `ability` WRITE;
/*!40000 ALTER TABLE `ability` DISABLE KEYS */;
INSERT INTO `ability` VALUES (5,4,10),(6,4,7),(7,4,8);
/*!40000 ALTER TABLE `ability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accompaniment`
--

DROP TABLE IF EXISTS `accompaniment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accompaniment` (
  `account` int(11) NOT NULL,
  `coach` int(11) NOT NULL,
  PRIMARY KEY (`account`,`coach`),
  KEY `fk_coach_accompaniment_idx` (`coach`),
  CONSTRAINT `fk_coach_accompaniment` FOREIGN KEY (`coach`) REFERENCES `coach` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_trainee_accompaniment` FOREIGN KEY (`account`) REFERENCES `trainee` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accompaniment`
--

LOCK TABLES `accompaniment` WRITE;
/*!40000 ALTER TABLE `accompaniment` DISABLE KEYS */;
INSERT INTO `accompaniment` VALUES (7,35),(8,35),(14,36),(17,36),(19,40),(21,40),(22,46),(24,46),(27,48),(32,48);
/*!40000 ALTER TABLE `accompaniment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `address` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_account` (`id`,`firstname`,`lastname`,`address`,`role`),
  UNIQUE KEY `unique_password` (`email`,`password`),
  KEY `fk_role_idx` (`role`),
  KEY `fk_address_idx` (`address`),
  CONSTRAINT `fk_address` FOREIGN KEY (`address`) REFERENCES `address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'Eliès','Cannell',1,1,'1997-06-30','c@free.fr','pouetpouet'),(2,'yan','nick',2,1,'2018-05-01','non@gmx.fr','blabla'),(4,'Pélagie','Bach',3,1,'1997-01-16','cbach3@technorati.com','35luoviAzUG'),(5,'Maï','Warin',4,5,'1982-11-13','awarin4@shop-pro.jp','R06PW8iqZa'),(6,'Cunégonde','Simonin',5,3,'1969-04-21','ksimonin5@nhs.uk','rWxKSoPh'),(7,'Lén','O\'Dulchonta',6,2,'1986-12-24','modulchonta6@japanpost.jp','dLbTCi1Knh8J'),(8,'Cinéma','Bodesson',7,2,'1985-01-20','ebodesson7@tmall.com','HOFlzjY'),(9,'Pò','Hackwell',8,3,'1981-08-09','dhackwell8@wisc.edu','PDQBgNbh2QE'),(11,'Léonore','Bodker',9,5,'1998-12-03','sed@free.fr','pouuetpouet'),(12,'Björn','Blabey',10,3,'1971-09-02','eblabeyb@bloglines.com','5UUJCCeEOj'),(13,'blabli','truc',10,3,'2018-05-01','fsdfsdf@gfdgd','dgdfgdfgdf'),(14,'Intéressant','De la Yglesia',14,2,'1993-03-24','cdelayglesia3@ca.gov','molestie'),(15,'Loïs','Forcer',15,5,'1981-04-11','hforcer4@ihg.com','lectus'),(16,'Cléopatre','MacMillan',16,1,'1983-12-01','mmacmillan5@slideshare.net','in'),(17,'Bérengère','Quare',17,2,'1992-01-14','mquare6@bbb.org','augue'),(18,'Rébecca','Gilleon',18,5,'1986-01-27','egilleon7@geocities.jp','sed'),(19,'Pélagie','Lodwick',19,2,'1973-05-23','hlodwick8@amazon.co.jp','id'),(20,'richard','steimer',2,4,'2010-02-02','oiu@free.fr','pouetpouet'),(21,'Léa','Burt',11,2,'1977-04-02','bburt0@ycombinator.com','et'),(22,'Régine','Spragg',11,2,'1979-01-10','cspragg0@shinystat.com','vBDz1rf2H'),(23,'Torbjörn','Sealy',13,5,'1995-09-27','TS@free.fr','pouetpouet'),(24,'Nélie','Wretham',14,2,'1972-06-20','ewretham2@purevolume.com','HdHt1aUe'),(25,'Naëlle','Antognoni',9,4,'1998-12-27','cantognoni3@ustream.tv','pouetpouet'),(26,'Maëlyss','Rabbatts',18,1,'1987-12-13','orabbatts4@nytimes.com','sjvCdZkl'),(27,'Maëlla','Entwistle',2,2,'1986-09-08','aentwistle5@homestead.com','XDgd45'),(28,'Mélia','Rawlin',6,4,'1976-02-23','lrawlin6@unc.edu','pouetpouet'),(29,'Yè','Spinozzi',1,5,'1970-04-01','jspinozzi7@comcast.net','cRfTkkg0We'),(30,'Ophélie','Padrick',1,1,'1970-12-04','bpadrick8@sogou.com','vWow2omwiP'),(31,'Gwenaëlle','Tevelov',9,4,'1979-10-28','ktevelov9@1und1.de','pouetpouet'),(32,'Aí','Mancktelow',4,2,'1996-02-19','emancktelowa@twitter.com','VgT2rg5Gf'),(33,'Maëly','Dilliston',8,5,'1969-11-16','gdillistonb@economist.com','h1p1Re0'),(34,'Mélissandre','Foakes',14,5,'1978-05-13','lfoakesc@tripadvisor.com','l8hFFO7'),(35,'Léone','McKew',15,3,'1971-03-18','cmckewd@ucla.edu','XiuktVJXU'),(36,'Jú','Gannicleff',16,3,'1991-04-12','egannicleffe@mapquest.com','vxtAROA'),(37,'IBM','bah les ordi',5,7,'1984-02-29','rquilterf@geocities.com','IBM'),(38,'Marie-thérèse','Tarry',6,1,'1992-06-04','dtarryg@telegraph.co.uk','MO51TMmFh'),(39,'little Italie','pizzeria',8,7,'1995-11-01','eknellenh@addthis.com','LI'),(40,'Håkan','Learmonth',5,3,'1984-05-30','jlearmonthi@cdc.gov','IlQwx5dQO4Me'),(41,'Cloé','MacAllaster',12,1,'1997-09-20','jmacallasterj@miibeian.gov.cn','xOY3qfOd'),(42,'renault','voitures',5,7,'1974-07-14','tbudgetk@google.co.jp','renault'),(43,'Björn','Cordrey',18,5,'2000-03-09','qcordreyl@howstuffworks.com','U4XKlv0ZpwT'),(44,'Océanne','Puncher',1,5,'1991-02-28','tpuncherm@photobucket.com','VuqIkONC'),(45,'Åslög','Croster',11,1,'1991-04-28','bcrostern@mashable.com','tD2oY01Z7PX'),(46,'Göran','Adamski',19,3,'1978-09-13','fadamskio@va.gov','9Fu8cE'),(47,'haribo','usine',4,7,'1977-06-07','azottop@illinois.edu','Haribo'),(48,'Amélie','Stanway',6,3,'1995-01-30','kstanwayq@intel.com','nt501zVZEvVf'),(49,'TF1','television',19,7,'1997-07-25','ddadger@photobucket.com','TF1'),(50,'Jú','Roomes',2,1,'1992-04-27','froomess@com.com','xv2QnaQm'),(51,'Stéphanie','Lissimore',15,3,'1982-11-07','clissimoret@devhub.com','S2FObknyFgP'),(52,'charly','boy',19,4,'1945-02-02','war@free.fr','pouetpouet');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_event`
--

DROP TABLE IF EXISTS `account_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `guest` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `state` enum('0','1','2','3') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account` (`account`),
  KEY `event` (`event`),
  KEY `guest` (`guest`),
  CONSTRAINT `account_event_ibfk_1` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `account_event_ibfk_2` FOREIGN KEY (`event`) REFERENCES `event` (`id`),
  CONSTRAINT `account_event_ibfk_3` FOREIGN KEY (`guest`) REFERENCES `account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_event`
--

LOCK TABLES `account_event` WRITE;
/*!40000 ALTER TABLE `account_event` DISABLE KEYS */;
INSERT INTO `account_event` VALUES (1,40,1,14,'2019-03-01 00:00:00','2019-03-02 00:00:00','0'),(2,6,2,7,'2019-03-01 00:00:00','2019-03-02 00:00:00','0'),(3,52,3,1,'2019-02-06 00:00:00','2019-02-06 00:00:00','0'),(4,52,3,2,'2019-02-06 00:00:00','2019-02-06 00:00:00','0'),(5,52,3,4,'2019-02-06 00:00:00','2019-02-06 00:00:00','1'),(6,23,4,23,'2019-03-27 17:25:00','2019-03-29 17:25:00','1'),(7,1,5,1,'2019-03-17 14:49:00','2019-03-19 14:49:00','1'),(8,1,6,1,'2019-03-17 14:49:00','2019-03-19 14:49:00','1'),(9,1,7,2,'2019-03-26 14:53:00','2019-03-27 14:53:00','2'),(10,1,7,2,'2019-03-28 02:54:00','2019-03-29 02:54:00','3'),(11,37,8,37,'2019-03-19 15:19:00','2019-03-20 15:19:00','1');
/*!40000 ALTER TABLE `account_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(45) NOT NULL,
  `number` varchar(10) NOT NULL,
  `type` int(11) NOT NULL,
  `additional` varchar(128) DEFAULT NULL,
  `zip_code` varchar(10) NOT NULL,
  `city` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_address_type_idx` (`type`),
  CONSTRAINT `fk_address_type` FOREIGN KEY (`type`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'craignosse','66',1,'coupe gorge','66666','lunel','france'),(2,'Tomscot','238',3,'Street','517','Bukabu','Indonesia'),(3,'Forest','2323',4,'Junction','50466','Shizikeng','China'),(4,'Dwight','0221',2,'Court','91664','El Arenal','Mexico'),(5,'Oak Valley','2948',5,'Trail','71770','Hoani','Comoros'),(6,'Meadow Ridge','24570',6,'Trail','P24','Kill','Ireland'),(7,'Onsgard','7617',3,'Crossing','92933','Lazaro Cardenas','Mexico'),(8,'Gulseth','75928',4,'Junction','55511','Tabunok','Philippines'),(9,'Red Cloud','20',5,'Hill','140482','Rassvet','Russia'),(10,'Eastlawn','93',3,'Road','87950','Comé','Benin'),(11,'de la soif','62',4,'Street','E91','Cluain Meala','Ireland'),(12,'de la republique','51',2,'Lane','40510','Lexington','United States'),(13,'Prairieview','74',4,'Junction','57256','Nagbukel','Philippines'),(14,'Jay','2',5,'Parkway','57793','Banaba','Philippines'),(15,'Del Sol','91',5,'Crossing','89145','Jingyao','China'),(16,'Village','33',4,'Point','19353','Wuxia','China'),(17,'Morningstar','39',2,'Park','93289','Qiaoshi','China'),(18,'Upham','8',5,'Park','33781','Kebon','Indonesia'),(19,'Armistice','62',5,'Alley','76508','Shimokizukuri','Japan'),(20,'Prentice','49',2,'Point','40490','Smedjebacken','Sweden');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrator` (
  `account` int(11) NOT NULL,
  PRIMARY KEY (`account`),
  CONSTRAINT `fk_administrator_salaried` FOREIGN KEY (`account`) REFERENCES `salaried` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (23);
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advertisement`
--

DROP TABLE IF EXISTS `advertisement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emitter` int(11) NOT NULL,
  `contract` int(11) NOT NULL,
  `title` tinytext,
  `description` longtext,
  `date` date NOT NULL,
  PRIMARY KEY (`id`,`emitter`),
  KEY `fk_advertissement_company_idx` (`emitter`),
  KEY `fk_advertissement_contract_idx` (`contract`),
  CONSTRAINT `fk_advertissement_company` FOREIGN KEY (`emitter`) REFERENCES `company` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_advertissement_contract` FOREIGN KEY (`contract`) REFERENCES `contract` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertisement`
--

LOCK TABLES `advertisement` WRITE;
/*!40000 ALTER TABLE `advertisement` DISABLE KEYS */;
INSERT INTO `advertisement` VALUES (3,39,6,'entreprise 39 propose 6','commence à l\'id 3','2019-03-03'),(4,37,1,'entreprise 37 propose contrat 1',NULL,'2019-03-12'),(5,47,5,'entrerprise 47 prpose contrat 5',NULL,'2019-03-19'),(6,37,1,'Test Offre','Test Creation offre','1960-01-01'),(7,37,5,'test 39','Test Stage 39','1960-01-01'),(8,39,2,'Sales Consultant','Test','2019-03-27'),(9,37,5,'Super Stage Back','Test','1960-01-01'),(10,37,1,'Sales Consultant','Test','1960-01-01'),(11,37,3,'de','des','1960-01-01'),(12,37,3,'des','desds','2019-03-28'),(13,37,4,'test','test','1960-01-01'),(14,37,1,'tet','Super description','2019-03-06'),(15,37,1,'ENFIN','ENFIN','1960-01-01');
/*!40000 ALTER TABLE `advertisement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advertissement_interest`
--

DROP TABLE IF EXISTS `advertissement_interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertissement_interest` (
  `advertissement` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `state` enum('1','2','3') DEFAULT NULL,
  PRIMARY KEY (`advertissement`,`account`),
  KEY `fk_account_advertissement_trainee_idx` (`account`),
  CONSTRAINT `fk_account_advertissement_trainee` FOREIGN KEY (`account`) REFERENCES `trainee` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_advertissement_interest` FOREIGN KEY (`advertissement`) REFERENCES `advertisement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertissement_interest`
--

LOCK TABLES `advertissement_interest` WRITE;
/*!40000 ALTER TABLE `advertissement_interest` DISABLE KEYS */;
INSERT INTO `advertissement_interest` VALUES (3,1,'1'),(3,7,'2'),(3,24,'3'),(3,27,'3'),(4,1,'3'),(4,8,'1'),(4,22,'1'),(5,8,'2'),(5,17,'2'),(5,32,'1');
/*!40000 ALTER TABLE `advertissement_interest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance` (
  `student` int(11) NOT NULL,
  `date` date NOT NULL,
  `am` tinyint(4) NOT NULL DEFAULT '0',
  `pm` tinyint(4) NOT NULL DEFAULT '0',
  `note` longtext NOT NULL,
  PRIMARY KEY (`student`,`date`),
  CONSTRAINT `fk_attendance_student` FOREIGN KEY (`student`) REFERENCES `student` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance`
--

LOCK TABLES `attendance` WRITE;
/*!40000 ALTER TABLE `attendance` DISABLE KEYS */;
INSERT INTO `attendance` VALUES (1,'2019-03-04',1,1,'Bien'),(1,'2019-03-05',0,1,'Bien surtout le matin'),(1,'2019-03-10',1,1,'rien '),(1,'2019-03-11',1,1,'toutjours rien'),(4,'2019-04-03',1,1,''),(16,'2019-02-06',1,1,'il a été absent');
/*!40000 ALTER TABLE `attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coach`
--

DROP TABLE IF EXISTS `coach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coach` (
  `account` int(11) NOT NULL,
  PRIMARY KEY (`account`),
  CONSTRAINT `fk_salaried_coach` FOREIGN KEY (`account`) REFERENCES `salaried` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coach`
--

LOCK TABLES `coach` WRITE;
/*!40000 ALTER TABLE `coach` DISABLE KEYS */;
INSERT INTO `coach` VALUES (6),(9),(12),(13),(35),(36),(40),(46),(48),(51);
/*!40000 ALTER TABLE `coach` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communication`
--

DROP TABLE IF EXISTS `communication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communication` (
  `message` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  PRIMARY KEY (`message`,`sender`,`receiver`),
  UNIQUE KEY `unique_sender` (`sender`,`message`),
  KEY `fk_message_sender_idx` (`sender`),
  KEY `fk_message_receivers_idx` (`receiver`),
  CONSTRAINT `fk_message_communication` FOREIGN KEY (`message`) REFERENCES `message` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_message_receivers` FOREIGN KEY (`receiver`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_message_sender` FOREIGN KEY (`sender`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communication`
--

LOCK TABLES `communication` WRITE;
/*!40000 ALTER TABLE `communication` DISABLE KEYS */;
INSERT INTO `communication` VALUES (1,1,23),(101,1,23),(102,1,23),(103,1,23),(104,1,23),(105,1,23),(2,4,8),(68,4,34),(83,4,17),(82,5,15),(17,7,18),(74,8,22),(15,9,15),(3,11,23),(52,11,24),(120,11,23),(121,11,23),(122,11,23),(123,11,23),(75,13,43),(29,14,28),(20,15,37),(9,16,4),(73,16,28),(20,17,49),(61,18,29),(80,20,43),(8,23,4),(45,23,18),(60,23,40),(106,23,1),(109,23,1),(111,23,1),(112,23,1),(113,23,1),(114,23,1),(115,23,1),(116,23,1),(124,23,11),(125,23,11),(128,23,44),(129,23,1),(42,24,9),(54,25,12),(71,26,27),(64,29,24),(126,30,1),(127,30,1),(51,31,9),(12,32,41),(66,35,31),(21,36,33),(50,37,38),(93,38,47),(4,40,12),(26,42,8),(24,44,35),(67,44,27),(107,44,23),(55,45,30),(18,46,45),(95,46,40);
/*!40000 ALTER TABLE `communication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `account` int(11) NOT NULL,
  `coach` int(11) NOT NULL,
  PRIMARY KEY (`account`,`coach`),
  KEY `fk_coach_company_idx` (`coach`),
  CONSTRAINT `fk_account_company` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_coach_company` FOREIGN KEY (`coach`) REFERENCES `coach` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (37,35),(39,36),(42,40),(49,46),(47,48);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext,
  `begindate` date DEFAULT NULL,
  `endingdate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract`
--

LOCK TABLES `contract` WRITE;
/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
INSERT INTO `contract` VALUES (1,'CDI','2019-08-08',NULL),(2,'CDD','2019-03-11','2019-03-30'),(3,'CDI','2019-04-01',NULL),(4,'CA','2019-03-14','2019-03-21'),(5,'Stage','2019-03-30','2019-05-30'),(6,'cdd',NULL,NULL);
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coordinator`
--

DROP TABLE IF EXISTS `coordinator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coordinator` (
  `account` int(11) NOT NULL,
  PRIMARY KEY (`account`),
  CONSTRAINT `fk_salaried_coordinator` FOREIGN KEY (`account`) REFERENCES `salaried` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coordinator`
--

LOCK TABLES `coordinator` WRITE;
/*!40000 ALTER TABLE `coordinator` DISABLE KEYS */;
INSERT INTO `coordinator` VALUES (20),(25),(28),(31),(34),(52);
/*!40000 ALTER TABLE `coordinator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext,
  `description` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'RDV info','rdv d\'info 1'),(2,'RDV de suivi de formation','ceci est le deuxieme evenemets'),(3,'meet\'up','convention PHP'),(4,'sqdf','sd'),(5,'Random on s\'en fou',''),(6,'Random on s\'en fou',''),(7,'TEst2',''),(8,'Test','Test');
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expertise`
--

DROP TABLE IF EXISTS `expertise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expertise` (
  `advertissement` int(11) NOT NULL,
  `skill` int(11) NOT NULL,
  PRIMARY KEY (`advertissement`,`skill`),
  KEY `fk_expertise_skill_idx` (`skill`),
  CONSTRAINT `fk_expertise_advertissement` FOREIGN KEY (`advertissement`) REFERENCES `advertisement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_expertise_skill` FOREIGN KEY (`skill`) REFERENCES `skill` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expertise`
--

LOCK TABLES `expertise` WRITE;
/*!40000 ALTER TABLE `expertise` DISABLE KEYS */;
INSERT INTO `expertise` VALUES (6,1),(7,1),(10,1),(11,1),(13,1),(14,1),(15,1),(5,2),(6,2),(7,2),(9,2),(10,2),(11,2),(13,2),(14,2),(6,3),(8,3),(10,3),(15,3),(4,4),(8,4),(4,5),(3,7),(5,7),(9,7),(12,7),(5,8),(12,8),(5,9),(9,9);
/*!40000 ALTER TABLE `expertise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formation`
--

DROP TABLE IF EXISTS `formation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext NOT NULL,
  `description` longtext NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `volume` decimal(6,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formation`
--

LOCK TABLES `formation` WRITE;
/*!40000 ALTER TABLE `formation` DISABLE KEYS */;
INSERT INTO `formation` VALUES (1,'Beweb','dev web','2019-09-13','2020-09-23',750.00),(2,'pizzaîolo','bah comme son nom l\'indique','2019-05-19','2020-09-05',1600.00),(3,'web dessign','apprendre a dessiner ','2019-06-23','2020-12-19',2000.00),(4,'CV','apprenez a postuler chez Paul','2019-08-04','2020-07-29',2100.00),(5,'secouriste','bouche a bouche.\r\nmasssage cardiaque\r\nappel du 18....','2019-05-19','2020-05-21',700.00),(6,'gestes et postures','preservation du dos\r\n\r\ntravailler c\'est la santé, ne rien faire c\'est la conserver !\r\nfaites bosser les autres !','2019-07-26','2019-04-11',721.00),(7,'soudure','apprenez a souder avec Loctite !','2019-04-16','2019-08-06',460.00),(8,'force de vente','Comment extorquer les mamies et dormir tranquille','2019-07-23','2019-05-30',50.00),(9,'assurance','apprenez a extorquer tout le monde (parents, vieux, freres, soeurs, amis....)\r\n\r\nmodule serenité en option + 500€ de main a main','2019-08-15','2020-01-10',1800.00),(10,'Legal','etude du code civil','2019-07-15','2020-07-13',1699.00);
/*!40000 ALTER TABLE `formation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leave`
--

DROP TABLE IF EXISTS `leave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `valid` enum('0','1','2') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_leave_salaried` (`account`),
  CONSTRAINT `fk_leave_salaried` FOREIGN KEY (`account`) REFERENCES `salaried` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leave`
--

LOCK TABLES `leave` WRITE;
/*!40000 ALTER TABLE `leave` DISABLE KEYS */;
INSERT INTO `leave` VALUES (1,13,'2019-03-05 00:00:00','2019-03-12 00:00:00','1'),(2,5,'2019-03-11 00:00:00','2019-03-20 00:00:00','2'),(3,6,'2019-03-13 00:00:00','2019-03-16 00:00:00','0'),(4,6,'2019-03-20 00:00:00','2019-03-23 00:00:00','1'),(5,13,'2019-03-12 00:00:00','2019-03-16 00:00:00','2'),(6,13,'2019-03-28 00:00:00','2019-03-31 00:00:00','1'),(7,23,'2019-03-28 00:00:00','2019-03-31 00:00:00','2'),(8,23,'2019-04-11 00:00:00','2019-04-19 00:00:00','0'),(9,23,'2019-05-26 00:00:00','2019-05-30 00:00:00','0');
/*!40000 ALTER TABLE `leave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(128) DEFAULT NULL,
  `body` longtext,
  `seen` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'messge 1','ceci est le message 1',0),(2,'message non lu 2','message non lu 2',0),(3,'message 3','lmdskfhqpdslkfkqdsmlkfj',0),(4,'mssage 4','slkdmffjmdlkj',0),(5,'Lotus','Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.',0),(6,'Saab','In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. ',0),(7,'Pontiac','Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.',1),(8,'Chevrolet','In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.',1),(9,'Lincoln','Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',0),(10,'GMC','Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.',0),(11,'Buick','Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.',0),(12,'Oldsmobile','Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',1),(13,'Volkswagen','Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.',0),(14,'Mercury','Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.',0),(15,'Toyota','Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',1),(16,'Ford','Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',1),(17,'Nissan','Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.',1),(18,'Mazda','Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.',1),(19,'Mercedes-Benz','Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',0),(20,'Pontiac','Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.',0),(21,'Toyota','Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.',0),(22,'Chevrolet','Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero.',0),(23,'Mitsubishi','Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.',0),(24,'Mitsubishi','Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',0),(25,'Subaru','Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.',1),(26,'Lamborghini','Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.',0),(27,'Maserati','Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.',0),(28,'Ford','Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.',1),(29,'Saturn','Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',0),(30,'Lincoln','In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante.',1),(31,'Nissan','Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',0),(32,'Nissan','Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.',1),(33,'Volkswagen','Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.',0),(34,'Maserati','Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',1),(35,'Volkswagen','Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',1),(36,'Volkswagen','Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.',0),(37,'Toyota','Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.',1),(38,'Toyota','Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.',1),(39,'Dodge','Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',0),(40,'Pontiac','Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.',1),(41,'Lincoln','Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.',0),(42,'Dodge','Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.',1),(43,'Lincoln','Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.',1),(44,'Subaru','Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.',0),(45,'Maybach','Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',1),(46,'Cadillac','Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.',0),(47,'Chevrolet','Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.',1),(48,'Mercury','Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.',0),(49,'Toyota','Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst.',1),(50,'BMW','Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.',1),(51,'Chevrolet','Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.',0),(52,'Chevrolet','Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.',0),(53,'Volvo','Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.',1),(54,'Dodge','Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.',1),(55,'Suzuki','Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl.',1),(56,'Audi','Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.',1),(57,'Toyota','Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.',1),(58,'Infiniti','Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.',0),(59,'Mercedes-Benz','Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',1),(60,'Mercedes-Benz','Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.',0),(61,'Jeep','Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.',1),(62,'Chevrolet','Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.',1),(63,'Dodge','Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',0),(64,'Porsche','Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum.',0),(65,'Audi','Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat.',0),(66,'GMC','Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.',1),(67,'Toyota','Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',0),(68,'Chevrolet','Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.',1),(69,'Porsche','Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna.',0),(70,'Nissan','Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',0),(71,'Lincoln','Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.',1),(72,'Mazda','Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.',0),(73,'GMC','Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.',1),(74,'Infiniti','Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.',1),(75,'Chevrolet','Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',1),(76,'Ford','In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum.',0),(77,'Ford','Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.',1),(78,'Lincoln','Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',0),(79,'Porsche','Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.',0),(80,'Toyota','Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.',0),(81,'Infiniti','In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.',1),(82,'Toyota','Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.',0),(83,'Saab','Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.',1),(84,'Chevrolet','Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.',1),(85,'Mitsubishi','Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat.',0),(86,'BMW','Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.',1),(87,'Mazda','Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.',1),(88,'Subaru','Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.',1),(89,'GMC','Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.',1),(90,'Mitsubishi','Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.',0),(91,'Lexus','Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.',1),(92,'Ford','Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',0),(93,'Acura','Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.',0),(94,'Mazda','Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.',1),(95,'Volkswagen','Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',1),(96,'Chevrolet','Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.',1),(97,'Audi','Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.',0),(98,'GMC','Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',1),(99,'Ram','Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.',0),(100,'Nissan','Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.',0),(101,'test1','test1',0),(102,'test2','test2',0),(103,'test3','test3',1),(104,'test4','test4',1),(105,'test5','test5',1),(106,'messsage de sealy a cannel','corps du message 106 a sealy de ??????? fantomas ',0),(107,'nouveau test','voici le corps du dernier message 107',0),(109,'salut','ceci est le body du ...',0),(111,'salut2','ceci est mon corps',0),(112,'messsage de sealy a cannel','kjsfdlksdjfkljkdslkdkfjklfdskljfdkljs',0),(113,'messsage de sealy a cannel','jkhjkkjkjkkhkjkjhkkhkjhkh',0),(114,'messsage de sealy a cannel','fodksfsjsfmlksfmdlkslfmlmsd',0),(115,'messsage de sealy a cannel','fksmldlmksdqlkmdqsklmlqsdklkmqsdlkmqds',0),(116,'messsage de sealy a cannel','coucoucoucoyucoucou',0),(120,'message 3','ceci est un nouveau test',0),(121,'message 3','dernier',0),(122,'message 3','message bien envoyé',0),(123,'message 3','messsage 4',0),(124,'message 3','test d\'envoi de message',0),(125,'message 3','test d\'envoi de message',0),(126,'Saab','test de reponse SAAB\n ',0),(127,'Saab','mlkqsdfjf',0),(128,'nouveau test','test puncher',0),(129,'test2','dbdb',0);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext,
  `description` longtext,
  `formation` int(11) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `volume` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_formation_idx` (`formation`),
  CONSTRAINT `fk_formation` FOREIGN KEY (`formation`) REFERENCES `formation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` VALUES (1,'1er module','module 1',1,'2019-03-03','2019-03-06',21.00),(2,'module 2','module 2',1,NULL,NULL,14.00),(3,'module 3','module 3',2,NULL,NULL,35.00),(4,'module 5','module 5',3,NULL,NULL,21.00),(5,'module 6','module 6',3,NULL,NULL,14.00);
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` tinytext NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Stagiaire F',''),(2,'Stagiaire A',''),(3,'Accompagnateur',''),(4,'Coordinateur',''),(5,'Formateur',''),(6,'RH',''),(7,'Entreprise',''),(8,'Admin','');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salaried`
--

DROP TABLE IF EXISTS `salaried`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salaried` (
  `account` int(11) NOT NULL,
  PRIMARY KEY (`account`),
  CONSTRAINT `fk_account_salaried` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salaried`
--

LOCK TABLES `salaried` WRITE;
/*!40000 ALTER TABLE `salaried` DISABLE KEYS */;
INSERT INTO `salaried` VALUES (5),(6),(9),(11),(12),(13),(15),(18),(20),(23),(25),(28),(29),(31),(33),(34),(35),(36),(40),(43),(44),(46),(48),(51),(52);
/*!40000 ALTER TABLE `salaried` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` tinytext NOT NULL,
  `description` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (1,'IBM Certified','Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.'),(2,'PHP','Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.'),(3,'JS','Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.'),(4,'html','Vivamus in felis eu sapien cursus vestibulum.'),(5,'CSS','Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.'),(6,'jquerry','Morbi porttitor lorem id ligula.'),(7,'MySQL','Vivamus vel nulla eget eros elementum pellentesque.'),(8,'bootstrap','Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.'),(9,'symfony','Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.'),(10,'Angular\r\n','Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.');
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `formation` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  PRIMARY KEY (`formation`,`account`),
  KEY `fk_formation_idx` (`formation`),
  KEY `fk_student_trainee_idx` (`account`),
  CONSTRAINT `fk_formation_trainee` FOREIGN KEY (`formation`) REFERENCES `formation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_trainee` FOREIGN KEY (`account`) REFERENCES `trainee` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,1),(1,2),(2,4),(2,26),(3,30),(3,38),(4,41),(5,45),(5,50),(8,16);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainee`
--

DROP TABLE IF EXISTS `trainee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainee` (
  `account` int(11) NOT NULL,
  PRIMARY KEY (`account`),
  UNIQUE KEY `account_UNIQUE` (`account`),
  CONSTRAINT `fk_trainee_account` FOREIGN KEY (`account`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainee`
--

LOCK TABLES `trainee` WRITE;
/*!40000 ALTER TABLE `trainee` DISABLE KEYS */;
INSERT INTO `trainee` VALUES (1),(2),(4),(7),(8),(14),(16),(17),(19),(21),(22),(24),(26),(27),(30),(32),(38),(41),(45),(50);
/*!40000 ALTER TABLE `trainee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainer`
--

DROP TABLE IF EXISTS `trainer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainer` (
  `formation` int(11) NOT NULL DEFAULT '0',
  `account` int(11) NOT NULL,
  `coordinator` int(11) NOT NULL,
  PRIMARY KEY (`formation`,`account`,`coordinator`),
  KEY `fk_formation_idx` (`formation`),
  KEY `fk_coordinator_trainer_idx` (`coordinator`),
  KEY `fk_salaried_trainer_idx` (`account`),
  CONSTRAINT `fk_coordinator_trainer` FOREIGN KEY (`coordinator`) REFERENCES `coordinator` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_formation_trainer` FOREIGN KEY (`formation`) REFERENCES `formation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_salaried_trainer` FOREIGN KEY (`account`) REFERENCES `salaried` (`account`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainer`
--

LOCK TABLES `trainer` WRITE;
/*!40000 ALTER TABLE `trainer` DISABLE KEYS */;
INSERT INTO `trainer` VALUES (1,6,25),(1,15,28),(1,36,31),(2,5,31),(2,11,28),(3,11,31);
/*!40000 ALTER TABLE `trainer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (1,'rue'),(2,'impasse'),(3,'boulevard'),(4,'avenue'),(5,'place'),(6,'chemin');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'formenscop-db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-26 12:20:47
