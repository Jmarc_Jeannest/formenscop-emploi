<div class="pre-loader"></div>
	<div class="header clearfix">
		<div class="header-right">
			<div class="brand-logo">
				<a href="/recherche_stagiaire">
					<img src="vendors/images/formenscop-logo.png" alt="" class="logo">
				</a>
			</div>
			<div class="menu-icon">
				<span></span>
			</div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon"><i class="fa fa-user-o"></i></span>
						<span class="user-name"><?= $user->getFirstname() ?> <?= $user->getLastname() ?></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="profile"><i class="fa fa-user-md" aria-hidden="true"></i> Profile</a>
						<a class="dropdown-item" href="logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
					</div>
				</div>
			</div>
			<div class="user-notification">
				<div class="dropdown">
					<a href="http://<?= $_SERVER['SERVER_NAME']?>/unreadmessages">
						<i class="fa fa-envelope-o" aria-hidden="true"></i>
						<span class="badge notification-active"></span>
						<i id="messages"></i>
					</a>
	

	
					<a  href="http://<?= $_SERVER['SERVER_NAME']?>/unvalidevents">
						<i class="fa fa-calendar-times-o" aria-hidden="true"></i>
						<span class="badge notification-active"></span>
						<i id="events"></i>
					</a>
				</div>

			</div>
		</div>
	</div>