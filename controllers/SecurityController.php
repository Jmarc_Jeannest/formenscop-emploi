<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAOAccount;

/**
 * Ceci est un exemple de contrôleur 
 * il charge le security middleware dans le constructeur
 * 
 *
 * @author loic
 */
abstract class SecurityController extends Controller
{
    protected $currentUser;
    /**
     * Le constructeur de la classe Controller charge les datas passées par le client,
     * Pour charger le security middleware, le contrôleur concret invoque la methode
     * @see \BWB\Framework\mvc\Controller::securityLoader() 
     * pour charger la couche securité.
     */
    function __construct()
    {
        parent::__construct();
        $this->securityLoader();
        if ($user = $this->security->acceptConnexion()) {
            $this->currentUser = (new DAOAccount())->getRealUser($user->username);
        } else {
            header("Location: http://" . $_SERVER['SERVER_NAME'] . "/loginpage");
        }
    }
}