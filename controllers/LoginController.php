<?php
namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAOAccount;

class LoginController extends Controller
{
    /**
     * Retourne la vue de login
     */
    public function getView()
    {
        $this->render("login");
    }

    /**
     * Mecanisme de login 
     * Si l'utilisateur convient pour la plateforme :
     * Les crédentials sont bons + il est sur la bonne plateforme 
     * On genere un token pour la secu
     */
    public function login()
    {
        $user = (new DAOAccount())->getUserForLogin(
            $this->inputPost()['email'],
            $this->inputPost()['password']
        );
        if (!$user) {
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/loginpage");
        } else {
            $this->securityLoader();
            $this->security->generateToken($user);
            header("Location: http://" . $_SERVER["SERVER_NAME"] . "/recherche_stagiaire");
        }
    }

    /**
     * Deconnection
     */
    public function logout()
    {
        $this->securityLoader();
        $this->security->deactivate();
        header("Location: http://" . $_SERVER["SERVER_NAME"] . "/loginpage");
    }
}