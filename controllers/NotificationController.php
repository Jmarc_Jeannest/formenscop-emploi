<?php

namespace BWB\Framework\mvc\controllers;
use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\dao\DAOEvent;
use BWB\Framework\mvc\dao\DAOMessage;

use BWB\Framework\mvc\models\Account;

class NotificationController extends SecurityController {

   

    public function __construct(){
        parent::__construct();  
    }
  
    

    // cette methode compte le nombre de messages non lus par l'utilisateur courrant
    public function countMessages(){
        echo count($this->currentUser->getUnReadMessages());
    }

    // cette methode compte le nombre d'evenements non lus par l'utilisateur courrant
    public function countEvenements(){
        echo count($this->currentUser->getUnValidEvents());
    }

}