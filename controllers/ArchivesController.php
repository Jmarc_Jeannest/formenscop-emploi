<?php
namespace BWB\Framework\mvc\controllers; // je déclare le namespace

use BWB\Framework\mvc\Controller; // je me sers des namespace déclarés par le use
use BWB\Framework\mvc\models\Offre;
use BWB\Framework\mvc\dao\DAOArchives;
use BWB\Framework\mvc\controllers\SecurityController; 



class ArchivesController extends SecurityController {


    
        protected $dao;

    /**
     * Le constructeur de la classe Controller charge les datas passées par le client,
     * Pour charger le security middleware, le contrôleur concret invoque la methode
     * @see \BWB\Framework\mvc\Controller::securityLoader() 
     * pour charger la couche securité.
     */
    function __construct() {
        parent::__construct();
        $this->dao = new DAOArchives() ;
        
    }



    /**
     * Simule un utilisateur qui se loggue.
     * On utilise l'objet {@see DefaultModel} qui retourne des valeurs par defaut
     * pour la generation du token. 
     * 
     * @link /login URI definie dans le fichier config/routing.json     * 
     * 
     */
    public function login() {
        $this->security->generateToken(new DefaultModel());
        header("Location: http://" . $_SERVER['SERVER_NAME'] . "/token");
    }

    /**
     * Simule un utilisateur qui se deconnecte.
     * La metode effectue une redirection 
     * 
     * 
     * @link /logout URI definie dans le fichier config/routing.json  
     * 
     */
    public function logout() {
        $this->security->deactivate();
        header("Location: http://" . $_SERVER['SERVER_NAME'] . "/token");
    }
    
    /** 
     * Ici la methode sera invoquée lors d'une requête HTTP dont le verbe est DELETE. 
     * L'exemple retourne les données des propriétés put, post et get. 
     * 
     * N'hésitez pas tester !
     */
    public function delete(){
        var_dump($this->inputPut());
        var_dump($this->inputPost());
        var_dump($this->inputGet());
    }
    
    /**
     * La methode affiche les données variables de l'URI comme definies dans le fichier routing.json. 
     * 
     * 
     * @param type $value correspond a la partie variable de l'URI dont le pattern est : (:).
     * 
     * @example /api/default/bonjour retournera bonjour. 
     * @example /api/default/32 retournera 32. 
     */
    public function getByValue($value){
        echo "valeur passée dans l'uri : " . $value;
    }



    /**
     * Méthode qui récupère la vue des archives
     */
    public function getArchivesEmploi(){
        $this->render('archives-emploi');
    }


    /**
     * Méthode qui récupère les archives pour la vue 
     */
    public function getAllEmploi(){
        
        // $archives=array( // mets le tableau dans une variable archives
        //     'archivesEmploi'=>(new DAOArchives())->getAll() //"archivesEmploi" = nom de la clé => valeur c'est une ligne de la db
        //     // dans la méthode render du controller, la clé est tranformée en variable, et c'est de cette variable 
        //     // dont je me sers pour la foreach de la vue (énoter qu'il ne faut pas de "-" dans le nom)
        //     // Le getAll récupère ligne par ligne et les mets dans le tableau
        // );
        // $this->render('archives-emploi',$archives); // renvoie la vue avec le tableau
        // //render fait réf. à la méthode du controlleur. Elle prend en arguments la vue et les données
        
    /**
     * Méthode permettant de recuper tous les offres tous les skills et tous les contract
     * et de les envoyer à la vue
     */
   
        // $dao = new DAOArchives();
        
        $datas = 
        [
            "offres" => $this->dao->getAll(), // format clé valeur. Dans la vue, la foreach a besoin de la clé qui sera transformée en variable 
            "skills" => $this->dao->getSkills(),
            "contracts" => $this->dao->getContracts(),
            "user"=> $this->currentUser
        ];
        //  var_dump($datas);
        $this->render('archives-emploi',$datas);
    }
    
    

    /**
     * Méthode qui récupère la vue des archives par id
     */

    public function getArchivesEmploiId($id){
        $archives=(new DAOArchives())->retrieve($id); //instancie un nouveau dao et invoque la méthode retrieve du dao
        $jsonArchives = json_encode($archives); // json_encode retourne une valeur au format json 
        echo $jsonArchives; // le return ne passe pas donc echo
    }


    /**
     * Méthode qui permet la création d'une nouvelle offre
     */
    public function createNouvelleOffreEmploi(){
      
        $post = $this->inputPost(); // stocke dans $post les valeurs présentes dans l'inputPost

        $id= $this->currentUser->getUsername();
        $post["emitter"] = $id;
        $dao = new DAOArchives(); // instancie le dao archives
        $dao->create($post); // invoque la méthode create sur les données du $post
        
    }


    /**
     * Méthode qui récupère tous les skills 
     */
    public function getSkills(){
        $skills =(new DAOArchives())->getSkills(); //instancie un nouveau dao et invoque la méthode getSkills du dao
        $jsonSkills = json_encode($skills); // json_encode retourne une valeur au format json 
        echo $jsonSkills;
    }



}
