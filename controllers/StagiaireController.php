<?php
namespace BWB\Framework\mvc\controllers;
use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAOStagiaire;



/**
 * Ceci est un exemple de contrôleur 
 * il charge le security middleware dans le constructeur
 * 
 *
 * @author loic
 */
class StagiaireController extends SecurityController {
    

    /**
     * Le constructeur de la classe Controller charge les datas passées par le client,
     * Pour charger le security middleware, le contrôleur concret invoque la methode
     * @see \BWB\Framework\mvc\Controller::securityLoader() 
     * pour charger la couche securité.
     */
    function __construct() {
        parent::__construct();
        $this->dao = new DAOStagiaire();
    }

    /**
     * Retourne la vue recherche_stagiaire.php qui se trouve dans le dossier views.
     * 
     * @link / methode invoquée lors d'une requête à la racine de l'application
     */
    public function getStagiaire() {

        // Je declare une variable $s dans laquelle je stock un tableau 
        $s = array(

            "user"=>$this->currentUser, 
           
            // La chaine de caractere represente la vue dans laquelle on instensie un nouvelle objet DAOStagiaire auquel on attribue la method getAll    
            "recherche_stagiaire"=>(new DAOStagiaire())->getAll()
        );
        
        //Ici $this->render va servir a renvoyer sur notre vue les donnees qui sont recuperer dans notre objet DAOStagiaire auquel on attribuer la methode getAll()
        $this->render("recherche_stagiaire", $s);      
    }

    
    

}