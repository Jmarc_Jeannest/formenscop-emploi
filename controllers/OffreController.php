<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\models\Offre;
use BWB\Framework\mvc\dao\DAOOffre;
use BWB\Framework\mvc\controllers\SecurityController; 

/**
 * Le controleur Offre permet de fournir l'interaction necessaire au fonctionnement de la vue offresEmploi
 * Exemple il met en lien les requetes Ajax du front avec le DAO et donc la BDD du back
 * 
 * il NE charge pas le security middleware dans le constructeur
 *
 * @author Jmarc
 */
class OffreController extends SecurityController
    {
        protected $dao;
        protected $id;

    /**
     * Le constructeur de la classe Controller charge les datas passées par le client,
     * Pour charger le security middleware, le contrôleur concret invoque la methode
     * @see \BWB\Framework\mvc\Controller::securityLoader() 
     * pour charger la couche securité.
     */
    function __construct() {
        parent::__construct();
        //$this->securityLoader();
        $this->dao = new DAOOffre();

    }

    /**
     * Retourne la vue default.php qui se trouve dans le dossier views.
     * 
     * @link / methode invoquée lors d'une requête à la racine de l'application
     */
    public function getRecherche() {

        $this->render("recherche_stagiaire");
    }

    public function getAgendaViews() {
        $this->render("agenda-views");
    }


    
    /** 
     * Ici la methode sera invoquée lors d'une requête HTTP dont le verbe est DELETE. 
     * L'exemple retourne les données des propriétés put, post et get. 
     * 
     * N'hésitez pas tester !
     */
    public function delete(){
        var_dump($this->inputPut());
        var_dump($this->inputPost());
        var_dump($this->inputGet());
    }
    
    /**
     * La methode affiche les données variables de l'URI comme definies dans le fichier routing.json. 
     * 
     * 
     * @param type $value correspond a la partie variable de l'URI dont le pattern est : (:).
     * 
     * @example /api/default/bonjour retournera bonjour. 
     * @example /api/default/32 retournera 32. 
     */
    public function getByValue($value){
        echo "valeur passée dans l'uri : " . $value;
    }



    /**
     * Méthode permettant de recuper tous les offres tous les skills et tous les contract
     * et de les envoyer à la vue
     */
    public function getOffresEmploi()
    {
        $user = $this->currentUser;
        $id = $user->getUsername();
        $offres = $this->dao->getOffres($id);
        $skills = $this->dao->getSkills();
        $contracts = $this->dao->getContracts();
        $datas = 
        [
            "user"=>$user,
            "offres" => $offres,
            "skills" => $skills,
            "contracts" => $contracts,
            "id" => $id
        ];
        $this->render("offresEmploi", $datas);
    }



    /**Recupération d'une offre par son id */
    public function getOffreByID($id)
    {
        $offre = $this->dao->retrieve($id);
        $json = json_encode($offre);
        echo $json;
    }

    /**Recupération d'une offre par son id */
    public function updateOffreByID($array)
    {       
        $datas = $this->inputPut();
        $dao = new DAOOffre();
        $this->dao->update($datas);
    }

    /**Recupération d'une offre par son id */
    public function createOffre()
    {   
        $datas = $this->inputPost();

        $datas['emitter'] = $this->currentUser->getUsername();

        $this->dao->create($datas);
    }

    public function archiveOffreByID($id)
    {
        $this->dao->delete($id);
    }
}
