

/**
 * Gestion de datatable : bouton "nouvelle offre", barre de recherche, options de la table
 */

$('document').ready(function(){

    $('#tableArchive').DataTable({
        scrollCollapse: true, // autorise la table à réduire la hauteur quand un nb limité de lignes est affiché
        autoWidth: false, // active ou désactive la largeur automatique des colonnes
        responsive: true,
        columnDefs: [{ // permet de définir les options souhaitées des colonnes
            targets: "datatable-nosort",
            orderable: false,
        }],
        // dom: 'Bfrtip',
        // buttons: [
        //     {
        //         // text:'Nouvelle offre', // créé le bouton nouvelle offre
        //         // action:function(){
        //         //     getSkills(), // appelle la récup des skills au onclick
        //         //     $('#nouvelleOffreModale').modal('toggle') // ouvre la modale pour créer des offres
                    
        //         }
        //     }
        // ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "language": {
            "info": "_START_-_END_ of _TOTAL_ entries",
            searchPlaceholder: "Search" // barre de recherche 
        }
        
    });
});

// DOM : datatable ajoute un nb d'éléments à la table pour son contrôle et son affichage . 
// La position de ces éléments à l'affichage sont contrôlés par une combinaison de leur ordre 
// dans le document (DOM) et le css appliqué aux éléments
// dom : brftip
// B : extension du bouton qui indique à datatables où mettre chaque élément dans le DOM qu'il créé
// F : Filtre des inputs
// R : Affichage des éléments 
// T : La table 
// I : Sommaire de la table d'information
// P : Contrôle de la pagination 


/*****************************************************
 * Gestion des nouvelles offres                      *
 * Bouton "nouvelle offre" de la vue                 *
 *****************************************************/


function getSkills(skills){


    $.ajax({
        type: "GET",
        url: "/archives-emploi/skills", // pointe sur la route pour récupérer les skills

        success: function (response) {
            
            var skills =JSON.parse(response);
            for(i=0;i<skills.length;i++){ // boucle qui parcourt le tableau des skills en fonction de sa longueur
                var baliseHtml = document.createElement("option"); // créé la balise option
                baliseHtml.setAttribute("value", skills[i]["id"]); // définit l'attribut value  de la balise option à l'index courant
                baliseHtml.innerHTML = skills[i]["designation"]; // insère les designations des skills dans les balises à l'index courant
                document.getElementById("skillsN").appendChild(baliseHtml); // Insère les balises dans le DOM
                // console.log(baliseHtml); // affiche les id du tableau value
            }
             
        }
    });
}


function creationNouvelleOffre(response){

    // document.getElementById("formulairePost").reset();
    

    $('.date-picker').datepicker({ // modifie le format de date pour qu'il colle à la db
        language: 'en',
        autoClose: true,
        dateFormat: 'yyyy-mm-dd',
    });

    // $('#formulairePost').trigger("reset"); // efface les anciennes données du formulaire liées à l'ancien envoi
    // implémenter le trigger apres pour ne pas conserver l'ancienne date de datepicker 


    // let offre = $("#formulairePost").serialize(); //.serialize : encode les éléments en chaine de caractères pour submission
    // console.log(offre);
    
    // $.ajax({
    //     type: "POST",
    //     url: "/api/archives-emploi",
    //     data: offre,
    //     dataType: "text",
    //     success: function (response) {
    //         console.log(response);
    //         alert('Annonce envoyée avec succès');
            
    //     },
    //     error: function (error){
    //         console.log(error);
    //         alert("L'annonce n'a pas pu être transmise");
    //     }
    // });

        // Ajout d'un evenement click sur le button permettant d'envoyer la requete ajax de creation
        let btnCreate = document.getElementById("btnCreateOffre")

        btnCreate.addEventListener("click", function (e) {
    
            //Recupération des données du formulaire de Creation d'offre
            let dataForm = $("#formulairePost").serialize();
    
            //Requete Ajax type Post avec les données du formulaire
            $.ajax({
                type: "POST",
                url: "/api/archives-emploi",
                data: dataForm,
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (response) {
                    window.location = "/archives-emploi"
                },
                error: function (error) {
                    console.log("oupssssss")
                }
            });
        })
    
}


/******************************************************
 * Gestion de l'affichage d'une archive par la modale *
 * Bouton "voir" de la vue                            *
*******************************************************/


/***
 * Cette fonction est appelée dans voirOffreArchivee.
 * Elle affiche la modale et insère les valeurs dans 
 * les champs correspondants
 */
function archiveModale(response){ //fonction déclenchée au clic


    document.getElementById("title-consult").textContent = response.title;
    document.getElementById("date-consult").textContent = response.date;
    document.getElementById("contract-consult").textContent = response.contract.title;
    document.getElementById("description-consult").textContent = response.description;
    let skills = document.getElementById("skill-consult");
    //On vide la balise qui acceuillera les skills car sinon ils s'empilent quand on affiche plusieurs fois la modal
    skills.innerHTML = "";


    //on boucle sur la liste des skills et on créer un élement li qu'on intègre au DOM
    //a l'endroit souhaité
    for (let s of response.skills) {
        let li = document.createElement("li");
        li.textContent = s.designation;
        li.style.listStyle = "none"
        skills.appendChild(li);

    }

    $('#voirArchiveModale').modal('toggle'); // affichage de la modale. toggle : affiche ou cache des éléments
}

/***
 * Fonction qui est appelée dans la vue. Elle prend en argument l'event, et l'id passé en php
 * dans la vue
 */
function voirOffreArchivee(event, id){

    $.ajax({
        type: "GET",
        url: "/api/archives-emploi/"+id,
        success: function (response) {
        // console.log(response);
        parse = JSON.parse(response); //les données doivent être parsées pour être au bon format pour javascript
        archiveModale(parse);  // j'appelle la fonction archiveModale et lui passe en argument le résultat, la response parsée
        },
        error: function(error){
            console.log(error);
            alert("Votre annonce ne peut pas être affichée"); 
        }
    });

}


/***************************************************************
 * Gestion de l'affichage d'une archive pour recréér une offre * 
 * à partir d'une ancienne                                     *
 * Bouton "créer nouvelle" de la vue                           *
 ***************************************************************/



/**
 * Fonction qui affiche la modale et insère les valeurs retournées dans la fonction creationOffre
 * Elle est appelée dans creationOffre
 */
function dataOffre(response){ 

    
    document.getElementById("date-modif").textContent = response.date;
    document.getElementById("title-modif").value = response.title; //ok
    // document.getElementById("contract-modif").textContent = response.contract.title;
    document.getElementById("description-modif").textContent = response.description;//ok
    // let skills = document.getElementById("skill-modif");
    // //On vide la balise qui accueillera les skills car sinon ils s'empilent quand on affiche plusieurs fois la modal
    // skills.innerHTML = "";



    $('#modOffreModale').modal('toggle');
    let offre = $("#formulairePostArchive").serialize(); //.serialize : encode les éléments en chaine de caractères pour submission


    // je change le format de la date pour qu'il soit conforme à la db et que la requête puisse passer
    // ca écrase la date initiale de l'archive
    $('.date-picker').datepicker({
        language: 'en',
        autoClose: true,
        dateFormat: 'yyyy-mm-dd',
    });

}


/**
 * Récupère une offre par id
 * Fonction appelée au clic dans la vue
 */
function creationOffreParId(event, id){ // fonction au click, avec l'event et l'id passé en php dans la vue 

$.ajax({
    type: "GET",
    url: "/api/archives-emploi/"+id, // pointe la route api du routing.json
    
    success: function (response) {
        // console.log(response); 
        parse = JSON.parse(response); //analyse une chaîne de caractères JSON et construit la valeur JavaScript 
        dataOffre(parse); // appelle la methode et en argument le json parsé
        getSkills();
        var skills =[];
            for(i=0;i<skills.length;i++){ // boucle qui parcourt le tableau des skills en fonction de sa longueur
                var baliseHtml = document.createElement("option"); // créé la balise option
                baliseHtml.setAttribute("value", skills[i]["id"]); // définit l'attribut value  de la balise option à l'index courant
                baliseHtml.innerHTML = skills[i]["designation"]; // insère les designations des skills dans les balises à l'index courant
                document.getElementById("skill-modif").appendChild(baliseHtml); // Insère les balises dans le DOM
                console.log(baliseHtml); // affiche les id du tableau value
            }
    },
    error: function (error) {
        console.log(error);
        alert("Erreur : Votre offre n'a pas pu être créée");
    }
});
}

/**
 * Méthode qui envoie les offres via une ancienne archive
 */
function postViaArchive(){

    let archive = $("#postViaArchive").serialize(); //.serialize : encode les éléments en chaine de caractères pour submission
    
    $.ajax({
        type: "POST",
        url: "/api/archives-emploi",
        data: archive,
        dataType: "text",
        success: function (response) {
            console.log(response); 
            alert('Votre annonce a bien été envoyée');
        },
        error : function (error){
            console.log(error);
            alert("Votre annonce n'a pas pu être envoyée");
        }
    });
}







