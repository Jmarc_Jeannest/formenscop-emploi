$(function(){

   $('#table').DataTable({
    buttons: {
        buttons: [
            {
                text: 'Alert',
                action: function ( e, dt, node, config ) {
                    alert( 'Activated!' );
                    this.disable(); // disable button
                }
            }
        ]
    },
        searching: true,
        ordering : true
    });
})

table.buttons().container().appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );