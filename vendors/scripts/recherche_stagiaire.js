$('document').ready(function(){
    $('#tableStagiaire').DataTable({
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        columnDefs: [{
            targets: "datatable-nosort",
            orderable: false,
        }],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "language": {
            "info": "_START_-_END_ of _TOTAL_ entries",
            searchPlaceholder: "Search"
        },
    });
    
    /**
     * cette fonction sert a recuperer l'id de l'utilisateur courrant au moment du  
     * clic sur la petite enveloppe qui declenche la modal 
     */
    $("#tableStagiaire td > a ").click(function(e){
        let btn = e.currentTarget;
        let currentID = $(btn).parents("tr").attr("id");
        
    });

    /**
     * cette fonction sert a recuperer l'email de l'id selectionner est le place dans la modal pour qu'il s'affiche
     */
    $("#tableStagiaire td > a ").click(function(e){
        let btn = e.currentTarget; // creer une variable a laquelle on attribue le parametre de la function 
        let listeTD = $(btn).parents("tr").find("td");// je pointe la variable btn est je lui demande de d'aller chercher tout les td dans le parent tr
        let index = listeTD.length-2;// je creer une variable index est dans cette variable index je compte dans la liste listeTD pour pouvoir recuperer le td dont j'ai besion 
        let TD = listeTD[index];// je creer une variable TD dans laquelle j'affecte le tableau listeTD pour parametre index
        let email = $(TD).text();// je creer un variable email dans laquelle je pointe TD et je lui demande de me le retranscrire en text
        $("#mail").attr('value', email);// je pointe l'id de l'input dans lequelle je veut que s'affiche le mail de la personne est le lui attribue la valeur de la variable email
    });
});

