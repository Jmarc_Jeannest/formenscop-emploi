$(function(){  //permet d'attendre que le DOM soit chargé complètement

  $('#calendar').fullCalendar({ //pointe en Jquery l'id calendar et lui applique la méthode fullcalendar

  //içi Commence les options
    header:       //définition du header
    { 
      center: 'month,agendaWeek' // création d'un bouton au centre permettant de passer de la vue mois à semaine
    },
  
    views: 
    {
      month: // nom de la vue
      {
        titleFormat: 'DD,MM,YYYY' //formart
        // other view-specific options here
      },
      agendaWeek:
      {
        titleFormat: 'DD,MM,YYYY' //formart
      }
    },

    dayClick: function() //Permet de définir une fonction lorsqu'on clique sur un un jour du calendrier Comme par exemple afficher une modal
    {
      $('#bd-example-modal-lg').modal('toggle')
    }

  });

})