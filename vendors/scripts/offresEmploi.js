/**
 * Gestion des events sur boutons 
 */

//pour la modal Voir
function voirOffreModale(event, id) {

    getOffreByID(id)

}

//pour la modal modif
function modiOffreModale(event, id) {

    getOffreByIDForModif(id)
}

//pour archiver une offre
function archivage(event, id) {

    $('#conf').modal('toggle')

    $BtnOk = document.getElementById('okArchive')
    $BtnOk.addEventListener("click", function (e) {
        $.ajax({
            type: "DELETE",
            url: "/api/offres-emploi/"+id,
            success: function (response) {
                window.location = "/offres-emploi"
            },
            error: function (error) {
                console.log(oupssss)
            }
        });
    })

}

//pour créer une offre
function creerOffreModale() {

    document.getElementById("createForm").reset()


    // Ajout d'un evenement click sur le button permettant d'envoyer la requete ajax de creation
    let btnCreate = document.getElementById("btnCreateOffre")

    btnCreate.addEventListener("click", function (e) {

        //Recupération des données du formulaire de Creation d'offre
        let dataForm = $("#createForm").serialize()
        
        console.log(dataForm)

        //Requete Ajax type Post avec les données du formulaire
        $.ajax({
            type: "POST",
            url: "/api/offres-emploi/(:)",
            data: dataForm,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (response) {
                window.location = "/offres-emploi"
            },
            error: function (error) {
                console.log("oupssssss")
            }
        });
    })
}


/**
 * fonction appellé dans le sucess de la reuete Ajax.
 * qui permet de remplir les champs de la modal voir
 */
function voirModal(e) {
    //on pointe chaque élement que nous souhaitons peuplé
    document.getElementById("titleVoirModal").textContent = "- " + e.title
    document.getElementById("dateVoirModal").textContent = "- " + e.date
    document.getElementById("contractVoirModal").textContent = "- " + e.contract.title
    document.getElementById("descriptionVoirModal").textContent = "  " + e.description
    let skills = document.getElementById("skillVoirModal")
    //On vide la balise qui acceuillera les skills car sinon ils s'empilent quand on affiche plusieurs fois la modal
    skills.innerHTML = ""


    //on boucle sur la liste des skills et on créer un élement li qu'on intègre au DOM
    //a l'endroit souhaité
    for (let s of e.skills) {
        let li = document.createElement("li")
        li.textContent = "- " + s.designation
        skills.appendChild(li)

    }

    //Une fois la modal complété avec les élément de l'offre que l'on souhaite on l'affiche
    $('#voirOffreModale').modal('toggle');

}


/**
 * fonction appellé dans le sucess de la requete Ajax.
 * qui permet de remplir les champs de la modal modif
 * 
 * Mais vue qu'on ne peux pas faire d'UPDATE
 */
function modifModal(e, id) {
    console.log(e.emitter.account.id)
    //on pointe chaque élement que nous souhaitons peuplé
    document.getElementById("titleModifModal").setAttribute("value", e.title)
    document.getElementById("dateModifModal").setAttribute("value", e.date)

    //faire un une requete ajax pour recupérer tous les types de contrat
    //puis créer un select
    document.getElementById("contractModifModal").setAttribute("value", e.contract.title)

    //faire un une requete ajax pour recupérer tous les types de contrat
    //puis créer un select multiple
    document.getElementById("descriptionModifModal").textContent = e.description
    let skills = document.getElementById("skillModifModal")

    //on boucle sur la liste des skills et on créer un élement li qu'on intègre au DOM
    //a l'endroit souhaité

    //Affichage de la ModalVoir Offre
    $('#modifOffreModale').modal('toggle');

    //affectation des attributs à l'input sauvegarder Attebtion l'input submit doit être dans le form
    let btn = document.getElementById('btnModifSauv')
    btn.setAttribute("formaction","/api/offres-emploi/put/"+id)
    btn.setAttribute("formmethod","POST")

    modifSauv(id, e.emitter.account.id)

}

/**
 * ne fonction pas en php $_POST est vide
 * même dans PostMan
 */
function modifSauv(id, emitter) {
    let btn = document.getElementById('btnModifSauv')
    btn.addEventListener("click", function (e) {

        datas = $('#modifForm').serialize()
        datas += "&emitter=" + emitter


        $.ajax({
            url: "/api/offres-emploi/" + id,
            type: "PUT",
            data: datas,
            success: function (response) {
                console.log("ça marche")

            },
            error: function (error) {
                console.log("Oupssssss")
            }
        })
    })
}

/**Requete AJAX */

/**
 * 
 * function qui s'execute en cliquant sur le bouton voir
 * Envoie une requete ajax à php qui lui retourne en réponse une offre
 * 
 **/

function getOffreByID(id) {
    $.ajax({
        type: "GET",
        url: "/api/offres-emploi/" + id,
        success: function (response) {
            var json = JSON.parse(response)
            offre = json
            voirModal(offre)

        },
        error: function (error) {
            console.log("T'es pas un Dev Js")
        }
    });
}

/**
 * 
 * function qui s'execute en cliquant sur le bouton modif
 * Envoie une requete ajax à php qui lui retourne en réponse une offre
 * 
 **/

function getOffreByIDForModif(id) {
    $.ajax({
        type: "GET",
        url: "/api/offres-emploi/" + id,
        success: function (response) {
            var json = JSON.parse(response)
            offre = json
            modifModal(offre, id)

        },
        error: function (error) {
            console.log("T'es pas un Dev Js")
        }
    });
}

/**
 * Requete Ajax permettant de recupérer les contrats
 */

function getContract() {
    $.ajax({
        type: "GET",
        url: "/api/data/contract",
        success: function (response) {
            json = JSON.parse(response)

        },
        error: function (error) {
            console.log("T'es pas un Dev Js")
        }
    });
}


/**
 * Requete Ajax permettant de recupérer les skills
 */
function getSkills() {
    $.ajax({
        type: "GET",
        url: "/api/data/skills",
        success: function (response) {
            json = JSON.parse(response)

            //comment je fais pour stocker cette valeur dans une requête
            //sinon je prépare déjà le Select en PHP

        },
        error: function (error) {
            alert("T'es pas un Dev Js")
        }
    });
}

/**
 * Gestion de la liste sur la page principale
 */

$('document').ready(function () {
    $('#tableOffre').DataTable({
        responsive: true,
        dom: 'Bfrtip',
        //creation d'un boutons permettant d'afficher la modal
        buttons: [
            {
                text: 'Nouvelle offre',
                action: function () {
                    $('#creationOffreModale').modal('toggle')
                    creerOffreModale()
                }
            }
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "language": {
            "info": "_START_-_END_ of _TOTAL_ entries",
            searchPlaceholder: "Search"
        }

    });

    $("#voirOffreModale").click(function(e){
        let target = e.currentTarget; // recupère l'element lié à l'évenement click depuis l'id tableOffre dans un a compris dans un td
        let listesTD = $(target).parents("tr").find("td");// Pointe l'élement Target remonte sur le parent puis chercher tous les TD
        console.log(listesTD);
        //let index = listesTD.length-5;// Dans notre liste de TD il faut que l'on récupère 
        //let TD = listeTD[index];// je creer une variable TD dans laquelle j'affecte le tableau listeTD pour parametre index
        //let email = $(TD).text();// je creer un variable email dans laquelle je pointe TD et je lui demande de me le retranscrire en text
        //$("#mail").attr('value', email);// je pointe l'id de l'input dans lequelle je veut que s'affiche le mail de la personne est le lui attribue la valeur de la variable email
    });

});

