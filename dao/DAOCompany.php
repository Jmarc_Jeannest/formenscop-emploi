<?php
namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOCompany extends DAO
{
   //put your code here
   public function create($array)
   { }


   public function update($array)
   { }

   public function delete($id)
   { }

   public function getAll()
   { }

   public function getAllBy($filter)
   { }
   /**
    * L'implémentation de retrieve est juste là pour coller au CRUD
    * C'est juste un select *
    * L'idée est que finalement la classe Account aura toutes ses propriétés alimentées.
    */
   public function retrieve($id)
   {
   }

}