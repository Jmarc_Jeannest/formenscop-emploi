<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\DAO;
use PDO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DAOOffre
 * 
 * Le DAOOffre, permet de récupérer des informations lié au offres dans la BDD
 *
 * @author Jmarc
 */
class DAOSkill extends DAO
{

    public function create($array)
    {
        
        $pdo = $this->getPdo();
        $stat = $pdo->prepare("INSERT INTO skill (designation, description) VALUES ('{$array['designation']}','{$array['description']}')");
        $stat->execute();

        return $stat;

    }

    public function delete($id)
    {
        $pdo = $this->getPdo();
        $stat = $pdo->query("SELECT * from  where id=".$id);

    }

    public function retrieve($id)
    {
        $pdo = $this->getPdo();
        $stat = $pdo->query("SELECT * from skill where id=".$id);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Skill");
        $skill = $stat->fetch();

        return $skill;
    }

    public function update($array)
    {

    }

    public function getAll()
    {

        $pdo = $this->getPdo();
        $stat = $pdo->query("SELECT * FROM skill");
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Skill");
        $skills = $stat->fetchAll();

        return $skills;

    }

    public function getAllBy($filter)
    {

    }

}