<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DAOOffre
 * 
 * Le DAOOffre, permet de récupérer des informations lié au offres dans la BDD
 *
 * @author Jmarc
 */
class DAOOffre extends DAO{

    //put your code here


    /**
     * Création d'une offre et des lien dans la table de correspondance expertise des skills
     * lié à cett offre
     * 
     * ATTENTION pas de gestion des erreurs
     * 
     */
    public function create($array) {

        $pdo = $this->getPdo();

        /* Patern à respecter
        INSERT INTO table (nom_colonne_1, nom_colonne_2, ...
        VALUES ('valeur 1', 'valeur 2', ...)
        */


        //Modification du format de la date de 01 March 2019 à 2019-03-01 
        $date = $this->dateFormat($array);

        //Sur l'objet pdo appel de la méthode prepare, avec en argument la requete permettant de créer une offre
        //Si la requete passe cela retourne un objet statement sinon un booleen a false
        $stat = $pdo->prepare("INSERT INTO advertisement (emitter, contract, title, description,  date) 
        VALUES ('{$array['emitter']}', '{$array['contract']}', '{$array['title']}', '{$array['description']}', '{$date}')");

        //Sur l'objet statement on appel la méthode execute qui execute la requete prepare
        $result = $stat->execute();

        //On récupère l'id du dernier element inseré en BDD
        $lastID = $pdo->lastInsertId();

        //On boucle sur tous les skills présent dans le tableau passé en argument de cette méthode
        //Pour preparer et executer a chaque skill la requete permettant de lié le skill et l'offre
        //via la table de correspondance expertise
        foreach($array['skill'] as $s)
        {
            $stat ="";
            $stat = $pdo->prepare("INSERT INTO expertise (advertissement, skill) VALUES ('$lastID', '$s' )");
            $stat->execute();
        }
        
    }

    /**
     * 
     * Dans le contexte actuel la date de l'input date n'est pas au format attendu par la BDD
     * cette méthode permet donc de 
     * 
     */
    private function dateFormat($array)
    {
        //on pointe la clef date dans le tableau, et on la decoupe grace à la fontion
        //date_parse
        $tabDate = date_parse($array['date']);

        //on pointe la clef month dans le tableau de date décomposé et on vérifie sa longueur
        //si c'est elle est égale à 1, on rajoute un 0 avant
        if (strlen($tabDate['month']) === 1)
        {
            $tabDate['month'] = "0".$tabDate['month'];
        }
        //Puis on reconstuit la date au format attendu par la BDD
        $date = $tabDate['year'].'-'.$tabDate['month'].'-'.$tabDate['day'];

        //on renvoie la date
        return $date;
    }
    
    
    /** 
     * ATTENTION ON NE PEUX PAS SUPPRIMER DANS LA BDD
     * 
     * On Utilise donc la méthode delete pour ARCHIVER une OFFRE
     * 
     * l'ARCHIVAGE PAR CONVENTION s'opère avec une affectation de la valeur de la colonne date de la table
     * Advertisement au '1960-01-01'
     * 
     * @param id (l'id de l'offre à supprimer)
     * 
     * */
    public function delete($id) 
    {

        $pdo = $this->getPdo();
        
        $stat = $pdo->prepare("UPDATE advertisement SET date = '1960-01-01' WHERE id={$id}");
        var_dump($stat);
        $stat->execute();
        
    }

    public function getAll() {}

    /**
     * Recupérer les offres
     * pour chaque offre recupérer emitter qui est l'id de compagny
     * pour chaque id de compagny on recupère le compte
     * on affecte account à id compagny
     * on affecte compagny à emitter de advertisement
     * 
     * pour chaque contrat on recupère l'objet contrat
     * et on assigne contrat à contre advertisement
     * 
     * on recupère la list des skills via l'id de Advertisement dans la table Expertise
     * pour chaque id de skill dans la table expertise on récupère l'objet skills
     * on affecte skill expertise l'objet skill récupérérer précédement
     * 
     * on affecter expertise à skill dans advertissement
     * 
     * on recupère la table advertissement_interest grace l'id advertissement
     * puis on refait la manipulation pour account
     * 
     * puis on renvoie le tout dans interrest de la table Advertissement
     * 
     * 
     */
    public function getOffres($id) {

    $stat = $this->getPdo()->query(" SELECT * from advertisement where date != '1960-01-01' and emitter = '$id' ");
    $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Advertisement");
    $advertisements = $stat->fetchAll();

    //var_dump($advertisements);

    //on parcours chaque Offre
    foreach($advertisements as $k => $e)
        {
            //on récupère chaque id d'emitter
            $idEmitter = $advertisements[$k]->getEmitter();

            //On recèpère une compagny par l'id de l'emitter et on l'affecte à l'advertissement courant à la place de l'id de l'emitter
            $company = $this->getCompanyByID($idEmitter);
            $advertisements[$k]->setEmitter($company);

            //On récupère un contrat par son id et on l'affecte à l'advertissement courant à la place de contract
            $idcontract = $advertisements[$k]->getContract();
            $contract = $this->getContractByID($idcontract);
            $advertisements[$k]->setContract($contract);

            //on récupère la liste des skills via la table de correspondance expertise
            $idAdvertisement = $advertisements[$k]->getId();
            $skills = $this->getSkillsByIDAdvertisement($idAdvertisement);
            $advertisements[$k]->setSkills($skills);

            // WARNING !!!!! Fonction en Pause car je ne peux pas mettre l'attribut dans Account
            //on recupère la liste des personnes interressé par l'annonce et on l'affecte 
            //$accountsInterest = $this->getAccsAndStateByIDAdvertisement($idAdvertisement);
            //$advertisements[$k]->setInterest($accountsInterest);

        }
        
        return $advertisements;

    }

    public function getAllBy($filter) {
        
    }

    /**
     * Méthode permettant de récupérer une offre sous forme d'objet en y incluant
     * des sous objets lié au FK. Si cette dernière nécessite d'étre sous forme d'objet
     * Exemple une Offre à un emitter qui est lui même une compagny qui est lui même composé d'un Account
     *  et d'un coach et qui est lui même composé d'un account ...
     * 
     */
    public function retrieve($id) {

        $stat = $this->getPdo()->query(" SELECT * FROM advertisement WHERE advertisement.id=".$id);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Advertisement");
        $advertisement = $stat->fetch();

        //on récupère chaque id d'emitter
        $idEmitter = $advertisement->getEmitter();

        //stockage de l'emitter pour une réutilisation ultérieure dans update et je ne souhaite pas le faire passer en Js on le mettre un input caché dans la modal de l'update de l'offre
        $_SESSION['emitter'] = $idEmitter;

        //On recèpère une compagny par l'id de l'emitter et on l'affecte à l'advertissement courant à la place de l'id de l'emitter
        $company = $this->getCompanyByID($idEmitter);
        $advertisement->setEmitter($company);

        //On récupère un contrat par son id et on l'affecte à l'advertissement courant à la place de contract
        $idcontract = $advertisement->getContract();
        $contract = $this->getContractByID($idcontract);
        $advertisement->setContract($contract);
        //on récupère la liste des skills via la table de correspondance expertise
        $skills = $this->getSkillsByIDAdvertisement($id);
        $advertisement->setSkills($skills);
        
        // WARNING !!!!! Fonction en Pause car je ne peux pas mettre l'attribut dans Account
        //on récupère la liste des personnes interressé + leur niveau d'interressement que l'on affecte
        //dans l'offre a la colonne Interest
        //$accountsInterest = $this->getAccsAndStateByIDAdvertisement($id);
        //$advertisement->setInterest($accountsInterest);
        
        // On retourne l'offre        
        return $advertisement;
    }


    /**
     * 
     * ATTENTION : Méthode remplacé par celle du dessus
     * 
     * permet de pointer tous les champs de la vue_offre dans la BDD
     * vue qui regroupe déjà les tables et sélectionne seulement les colonnes dont on a besoin :
     * 
     *  `advertisement`.`id` AS `id`,
     *  `advertisement`.`date` AS `date`,
     *  `advertisement`.`title` AS `title`,
     *  `advertisement`.`description` AS `description`,
     *  `account`.`firstname` AS `firstname`,
     *  `contract`.`title` AS `contract`,
     *  `advertisement`.`id` AS `idContract`,
     *  `skill`.`designation` AS `designation`
     * 
     */

    //public function retrieve($id) 
    //{
    //    $request = "select * from vue_offre where id=".$id;
    //    $pdo = $this->getPdo();
    //    $stat = $pdo->query($request);
    //    $result = $stat->fetchAll();
    //    var_dump($result);
    //    return $result;
    //
    //}



/**
 * 
 * ATTENTION : Appel de la méthode désactivé dans la vue car la BDD sur la table advertissement bloque
 * les UPDATE sur les FK
 * 
 */
    public function update($array) {
        
        $pdo = $this->getPdo();
        $request = "UPDATE vue_offre SET emitter = {$array['emitter']} ,
        date = {$array['date']} , title = {$array['title']},
        idContract = {$array['contract']}, description = {$array['description']}
        WHERE id={$array['id']}";

        $pdo->exec($request);

    }

    /**  
     * Cette fonction permet de la liste des compagnies sous forme d'objet
     * incluant les sous objets lié à la classe Compagny
     * Exemple à la place d'avoir l'id de la d'adresse nous aurons l'objet adresse
     * 
     **/
    public function getCompanys(){
        
    //Envoie de la requete et recupération sous forme de liste d'objet Compagny
        $pdo = $this->getPdo();
        $request="SELECT * from company ";
        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Company");
        $companys = $stat->fetchAll();

    //Parcours de la liste des compagnies
        foreach($companys as $k => $e)
        {

            //recupération de l'account en fonction de la colonne account  de chaque companie
            $account = $this->getAccountsByID($companys[$k]->getAccount());

            /**
             * 
             * Ne Marche plus avec la securité de LOIC
             * 
             * 
             */
            //recupération de l'adresse en fonction de la colonne adresse de chaque account
            //$adress = $this->getAddressByID($account->getAddress());

            //Affectation de l'attribut adresse avec l'objet Adresse récupéré à la ligne précédente
            //$account->setAddress($adress);

            //recupération du role en fonction de la colonne adresse de chaque account
            $role = $this->getRoleByID($account->getRoles());;

            //Affectation de l'attribut role avec l'objet role récupéré à la ligne précédente
            $account->setRole($role);

            //Affectation de l'attribut account avec l'objet Account peuplé lui même de ses sous objets
            $companys[$k]->setAccount($account);

            //Récupération de de l'objet Coach lié à l'id Coach de la compagny
            $coach = $this->getAccountsByID($company[$k]->getCoach());

            //A la place de l'id coach dans compagny on y affecte la valeur de la variable coach
            $company[$k]->setCoach($coach);

        }

        //on retourne les companys
        return $companys;
    }

    /**
     * 
     * Permet de récupérer une compagny sous forme d'objet,
     * incluant les sous objets qui lui sont propre
     * 
     */
    public function getCompanyByID($id){
        //Instanciation de PDO
        $pdo = $this->getPdo();

        //ecriture de la requete
        $request="SELECT * FROM company WHERE account=";

        //Exécution de la requête et recupération du résultat avec en format de sortie un objet Company
        $stat = $pdo->query($request.$id);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Company");
        $company = $stat->fetch();

        //initialisation de la variable $account avec l'objet Account correspondand à l'id de compagnie
        $account = $this->getAccountsByID($company->getAccount());

        /**
         * 
         * Ne Marche plus avec la securité de LOIC
         * 
         * 
         */
        
        /** initialisation de la variable $adresse avec l'objet adresse correspondand à l'id d'adresse 
         * dans account précédement récupéré */
        //$adress = $this->getAddressByID($account->getAddress());
        

        //A la place de l'id addresse dans l'account on y affecte l'objet addesse
        //$account->setAddress($adress);

        /** initialisation de la variable $role avec l'objet role correspondand à l'id d'role
         * dans account précédement récupéré */
        //$role = $this->getRoleByID($account->getRoles());

        //A la place de l'id role dans l'account on y affecte l'objet role
        //$account->setRole($role);


        //A la place de l'id account dans compagny on y affecte la valeur de la variable account
        $company->setAccount($account);

        //Récupération de de l'objet Coach lié à l'id Coach de la compagny
        $coach = $this->getAccountsByID($company->getCoach());

        //A la place de l'id coach dans compagny on y affecte la valeur de la variable coach
        $company->setCoach($coach);

        return $company;
    }

    /** Récupération d'un objet Adresse grâce son id */
    private function getAddressByID($id)
    {
        $pdo = $this->getPdo();
        $request="SELECT * from address where {$id}";
        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Address");
        $result = $stat->fetch();
        return $result;
    }

    /** Récupération d'un objet Role grâce son id */
    private function getRoleByID($id)
    {
        $pdo = $this->getPdo();
        $request="SELECT * from role where {$id}";
        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Role");
        $result = $stat->fetch();

        return $result;
    }

    /** Récupération d'un objet Account grâce son id */
    private function getAccountsByID($id)
    {
        $pdo = $this->getPdo();
        $request="SELECT * from account WHERE id={$id}";
        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Account");
        $account = $stat->fetch();

        return $account;
    }


    /**Récupération d'un objet Contract grâce son id */
    private function getContractByID($id)
    {
        $pdo = $this->getPdo();
        $request="SELECT * from contract WHERE id={$id}";
        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Contract");
        $contract = $stat->fetch();

        return $contract;
    }

    /**
     * Récupération de tous les contrats sous forme d'une liste d'objet
     * Cela est necessaire pour l'affichage de la modal de création
     */
    public function getContracts()
    {
        $pdo = $this->getPdo();
        $request="SELECT * from contract";
        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Contract");
        $contract = $stat->fetchAll();

        return $contract;
    }

    /**recupérer la liste d'expertise par id provenant de la table Advertisement */
    private function getExpertisesByID($id)
    {
        $pdo = $this->getPdo();
        $request="SELECT * from expertise WHERE advertissement={$id}";
        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Expertise");

        $expertises = $stat->fetchAll();

        return $expertises;
    }

    /**recupérer une liste de skill en fonction de leur lien avec une offre */
    private function getSkillsByIDAdvertisement($id)
    {

        $pdo = $this->getPdo();
        $request="SELECT skill.* from skill 
        INNER Join expertise on expertise.skill = skill.id
        WHERE expertise.advertissement={$id}";
        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Skill");

        $skills = $stat->fetchAll();

        return $skills;

    }

    /** 
     * WARNING !!!!! Fonction en Pause car je ne peux pas mettre l'attribut dans Account
     * recupère les Accounts et leur niveau d'interet sur une offre
     */
    private function getAccsAndStateByIDAdvertisement($id)
    {
        $pdo = $this->getPdo();

        $request="SELECT * from advertissement_interest
        WHERE advertissement_interest.advertissement={$id}";

        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Advertissement_interest");

        $interst = $stat->fetchAll();
        
        $result = [];

        //On parcours la liste d'objet interest et on construit un tableau result avec un objet Account
        //qui possède maintenant un attribut State
        foreach ($interst as $k => $e)
        {
            $accountID = $interst[$k]->getAccount();
            $account = $this->getAccountsByID($accountID);
            $accountState = $interst[$k]->getState();
            $account->setState_interest($accountState);
            array_push($result, $account);
        }

        return $result;
    }

    /**
     * Récupération de tous les skills sous forme d'une liste d'objet
     * Cela est necessaire pour l'affichage de la modal de création
    */
    public function getSkills()
    {
        $pdo = $this->getPdo();
        $request="SELECT * from skill";
        $stat = $pdo->query($request);
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Skill");

        $skills = $stat->fetchAll();

        return $skills;
    }
    
}