<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\DAO;
use PDO;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Le DAOStagiaire etends l'objet DAO
 *
 * @author loic
 */
class DAOStagiaire extends DAO{
    
    public function create($array) {
        
    }

    public function delete($id) {
        
    }

    /**
     * Cette fonction va servir a recuperer tout les stagiaires pour les afficher sur ma vue  
     */
    public function getAll() {

        // On fait une requête en 2 parties : 1-on récupère les trainees
        $stat = $this->getPdo()->query("SELECT account.* FROM account 
        INNER JOIN trainee ON account.id = trainee.account");
        $stat->setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Trainee"); // transforme le modèle en objet
        $trainees = $stat->fetchAll();
        $datas = []; // déclare un tableau vide pour y insérer les stagiaires avec leurs skills


        // Requête 2è partie : 2-on récupère les skills  
        foreach($trainees as $trainee){ // En faisant la boucle ici, je récupère les skills et les insère dans l'objet Trainee
            // pour chaque trainee, récupérer le skill
            $skill =$this->getSkills($trainee->getId()); //J'invoque getSkills sur l'objet Trainee avec en argument l'id du trainee courant
            $trainee->setAbilities($skill); // Affecte un skill sur les abilities du trainee (abilities est une propriété du modele Trainee)
            array_push($datas, $trainee); // insertion des données dans datas
        }

        return $datas;

        // A noter qu'il serait possible de faire une requête de ce type pour éviter une requête en 2 temps par foreach
        // $stat = $this->getPdo()->query("select account.*, group_concat(skill.designation separator ', ') from account
        // inner join trainee on trainee.account = account.id
        // inner join ability on trainee.account = ability.trainee
        // inner join skill on skill.id = ability.skill
        // group by account.id");

    }

    public function getAllBy($filter) {
        
    }

    public function retrieve($id) {
        
    }

    public function update($array) {
        
    }

    private function getSkills($idTrainee){
        // requete sql -> retourne le skill par son id 

        $pdo = $this->getPdo(); // récupère l'objet statement pdo 
        $sqlSkill = "SELECT skill.* FROM skill 
        INNER JOIN ability ON skill.id = ability.skill 
        INNER JOIN trainee ON ability.trainee = trainee.account
        WHERE ability.trainee=".$idTrainee;
        $statement = $pdo->query($sqlSkill); // invoque la méthode query sur $pdo
        $statement -> setFetchMode(PDO::FETCH_CLASS, "BWB\Framework\mvc\models\Skill"); // transforme le modele en objet 
        $result = $statement->fetchAll(); // invoque fetch sur statement pour l'affichage d'un élément unique 
        return $result;

    }

}
