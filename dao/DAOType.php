<?php
namespace BWB\Framework\mvc\dao;

use PDO;
use BWB\Framework\mvc\DAO;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DAODefault
 *
 * @author loic
 */
class DAOType extends DAO
{
    //put your code here
    public function create($array)
    { }

    public function delete($id)
    { }

    public function getAll()
    { }

    public function getAllBy($filter)
    { }

    public function retrieve($id)
    {
        $query = "SELECT * from type where id=" . $id;
        $statement = $this->getPdo()->query($query);
        if (!$statement) {
            return $this->getPdo()->errorInfo()[2]; //retourne juste le message
        } else {
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetch();
        }
    }

    public function update($array)
    { }
}