<?php
namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DAOAccount
 *
 * @author loic
 */
class DAOAccount extends DAO
{
    //put your code here
    public function create($array)
    { }


    public function update($array)
    { }

    public function delete($id)
    { }

    public function getAll()
    { }

    public function getAllBy($filter)
    { }
    /**
     * L'implémentation de retrieve est juste là pour coller au CRUD 
     * C'est juste un select * 
     * L'idée est que finalement la classe Account aura toutes ses propriétés alimentées.
     */
    public function retrieve($id)
    {
        $query = "SELECT * from account where id=" . $id;
        $statement = $this->getPdo()->query($query);
        if (!$statement) {
            return $this->getPdo()->errorInfo()[2]; //retourne juste le message
        } else {
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetch();
        }
    }
    /**
     * On remonte le nom de la classe qui correspond au bon utilisateur selon la position de son id.
     * On utilise le nom de la classe pour creer un nouvel utilisateur correct.
     * 
     */
    public function getRealUser($id)
    {
        $query = '
        select  
        case 
            when (exists 
                    (select coordinator.account from coordinator 
                    inner join account on coordinator.account = account.id 
                    where coordinator.account = ' . $id . ')
                ) = 1 
            then "Coordinator"
            when (exists 
                    (select trainer.account from trainer 
                    inner join account on trainer.account = account.id 
                    where trainer.account = ' . $id . ')
                ) = 1 
            then "Trainer"
            when (exists 
                    (select coach.account from coach 
                    inner join account on coach.account = account.id 
                    where coach.account = ' . $id . ')
                ) = 1 
            then "Coach"
            when (exists 
                    (select administrator.account from administrator 
                    inner join account on administrator.account = account.id 
                    where administrator.account = ' . $id . ')
                ) = 1 
            then "Administrator"
            when (exists 
                    (select student.account from student 
                    inner join account on student.account = account.id 
                    where student.account = ' . $id . ')
                ) = 1 
            then "Student"
            when (exists 
                    (select accompaniment.account from accompaniment 
                    inner join account on accompaniment.account = account.id 
                    where accompaniment.account = ' . $id . ')
                ) = 1 
            then "Accompaniment"
            when (exists 
                    (select company.account from company 
                    inner join account on company.account = account.id 
                    where company.account = ' . $id . ')
                ) = 1 
            then "Company"
        end as classname 
        from account 
        where account.id = ' . $id . ';
        ';
        $statement = $this->getPdo()->query($query);
        if (!$statement) {
            return $this->getPdo()->errorInfo()[2]; //retourne juste le message
        } else {
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $classname = "BWB\\Framework\\mvc\\models\\" . $statement->fetch()['classname'];
            return new $classname($id);
        }
    }

    /**
     * ici je vais faire le test pour les salariés donc 
     * je m'assure que l'id est bien dans la table salaried en plus d'avoir les bon crédentials
     */
    public function getUserForLogin($email, $password)
    {
        $query = $this->getQueryForLogin($email, $password);

        $statement = $this->getPdo()->query($query);
        if (!$statement) {
            return $this->getPdo()->errorInfo()[2]; //retourne juste le message
        } else {
            $statement->setFetchMode(PDO::FETCH_CLASS, "BWB\\Framework\\mvc\\models\\Account");
            return $statement->fetch();
        }
    }

    /**
     * La methode a ajuster selon les applications :
     * Changer le nom de la table pour quelle corresponde au bon type d'utilisateur
     */
    private function getQueryForLogin($email, $password)
    {
        return "SELECT * from account where 
        account.email='" . $email . "' and account.password='" . $password . "' and 
        exists (select account from company where company.account = 
            (select id from account where account.email='" . $email . "' and account.password='" . $password . "')
        )";
    }

    //cette methode sert a recuperer les messages non lus de l'utilisateur qui a pour id $id
    public function getAllByMessage($id) {
        $query="select *
        from communication 
        inner join message on communication.message=message.id 
        where communication.receiver=".$id." and message.seen=0;";
        $statement=$this->getPdo()->query($query);

        return $statement->fetchAll(PDO::FETCH_ASSOC); 
    }

    //cette methode sert a recuperer les evenements non lus de l'utilisateur qui a pour id $id
    public function getAllByEvent($id) {
        $query="select * 
        from event
        inner join account_event on account_event.event=event.id 
        where account_event.guest=".$id." and account_event.state=0;";
        $statement=$this->getPdo()->query($query);

        return $statement->fetchAll(PDO::FETCH_ASSOC); 
    }
}
