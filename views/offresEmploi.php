<!DOCTYPE html>
<html lang="fr">
<html>
<head>
	<?php include('include/head.php'); ?>

</head>
<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h2>Offres d'emploi</h2>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/recherche_stagiaire">home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Offres d'emploi</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<!-- Début de la table-->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<div class="clearfix mb-20">
						<div class="pull-left">
							<h3 class="font-23bdc3">Liste des offres</h3>
						</div>
					</div>
					<div class="row">
						<table id="tableOffre"class="data-table stripe hover nowrap">
							<thead>
								<tr>
									<th class="table-plus datatable-nosort">Date</th>
									<th>Intitulé de l'offre</th>
									<th>Type de contrat</th>
									<th>Description</th>
									<th>Compétences requises</th>
									<th class="datatable-nosort">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($offres as $e): ?>
								<tr>
									<td class="table-plus"><?= $e->getDate()  ?></td>
									<td> <?= $e->getTitle() ?> </td>
									<td> <?= $e->getContract()->getTitle() ?> </td>
									<td> <textarea class="textAreaDisable" disabled cols="40" rows="4"> <?= $e->getDescription() ?> </textarea> </td>
									<td>
										<ul>
										
										<?php foreach ($e->getSkills() as $s): ?>

											 <li><?= $s->getDesignation() ?></li>
										
										<?php endforeach ;?>
										</ul>
									</td>
									<td>
										<div class="dropdown">
											<a id="btnvoirOffreModale" class="btn btn-outline-primary dropdown-toggle" role="button" data-toggle="dropdown">
												<i class="fa fa-ellipsis-h"></i>
											</a>
											<div class="dropdown-menu">
												<a class="dropdown-item"  onclick = "voirOffreModale(event, <?= $e->getId() ?> )"><i class="fa fa-eye"></i> Voir</a>
												<!-- <a class="dropdown-item" onclick = "modiOffreModale(event, php $e->getId() php )"><i class="fa fa-pencil modifier-modale"></i> Modifier</a> -->
												<a class="dropdown-item"  onclick = "archivage(event, <?= $e->getId() ?> )"><i class="fa fa-archive"></i> Archiver</a>
											</div>
										</div>
									</td>
								</tr>
								<?php endforeach ;?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Fin de la table  -->
			<!-- Fermeture "min-height-200" -->
			</div>
			<!-- Fermeture customscroll -->
		</div>
		<!-- Fermeture "main container" -->
	</div>
			<!-- Large modal -->
	<div class="col-md-4 col-sm-12">
		<div class="modal fade bs-example-modal-lg" id="creationOffreModale" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title text-blue" id="myLargeModalLabel">Ajouter une offre</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">

						<!-- Default Basic Forms Start -->
						<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<div class="clearfix">
								<div class="pull-left">
									
								</div>
							</div>
							<form id=createForm>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Date</label>
									<div class="col-sm-12 col-md-10">
										<input name="date" id="dateCreateModal" class="form-control date-picker" placeholder="Date de Debut d'offre" type="text">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Offre</label>
									<div class="col-sm-12 col-md-10">
										<input name="title" id="titleCreateModal"class="form-control" type="text" placeholder="Intitulé de l'offre">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Type de contrat</label>
									<div class="col-sm-12 col-md-10">
										<select name="contract" id="contractCreateModal" class="custom-select col-12">
											<!-- insertion des types de contratc -->
										<?php
											foreach($contracts as $c){
												echo "<option value='".$c->getId()."'>".$c->getTitle()."</option>";
										} ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" id="descriptionCreateModal" class="form-control"></textarea>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Compétences requises</label>
									<div class="col-sm-12 col-md-10">
										<select name="skill[]" multiple id="skillCreationModal" class="custom-select col-12">

										<?php
											foreach($skills as $s){
												echo "<option value='".$s->getId()."'>".$s->getDesignation()."</option>";
										} ?>

										</select>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
						<button id="btnCreateOffre" type="button" class="btn btn-primary">Créer</button>
					</div>
				</div>
			<!-- Fermeture "min-height-200" -->
			</div>
		<!-- Fermeture customscroll -->
		</div>
	</div>

	<!-- debut Modification modal -->

	<div class="col-md-4 col-sm-12">
		<div class="modal fade bs-example-modal-lg" id="modifOffreModale" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myLargeModalLabel">Modifier une offre</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">

						<!-- Default Basic Forms Start -->
						<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">

							<form id="modifForm">
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Date</label>
									<div class="col-sm-12 col-md-10">
										<input name="date" id="dateModifModal" class="form-control date-picker" placeholder="Date de création" type="text">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Offre</label>
									<div class="col-sm-12 col-md-10">
										<input id="titleModifModal" name="title" class="form-control" type="text" placeholder="Intitulé de l'offre">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Type de contrat</label>
									<div class="col-sm-12 col-md-10">
										<select id="contractModifModal" name="contract" class="custom-select col-12">
										<?php
											foreach($contracts as $c){
												echo "<option value='".$c->getId()."'>".$c->getTitle()."</option>";
										} ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea id="descriptionModifModal" name=description class="form-control"></textarea>
								</div>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Compétences requises</label>
									<div class="col-sm-12 col-md-10">
										<!-- l'attribut name est suffixé de crochet [] pour que php sache que c'est un tableau -->
										<select id="skillModifModal" multiple name="skill[]" class="custom-select col-12">
										<!-- Construction des options en PHP -->
										<?php
											foreach($skills as $s){
												echo "<option value='".$s->getId()."'>".$s->getDesignation()."</option>";
										} ?>
										</select>
									</div>
								</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
						<!--
							Commentaire de la fonction la BDD ne pouvant permettre de modifier une Offre 
							<input id="btnModifSauv" type="submit" class="btn btn-primary" value="Sauvegarder">
						 -->
						<button id="btnModifSauv" type="button" class="btn btn-primary">Sauvegarder</button>
					</div>
					</form>
				</div>

			</div>

		</div>

	</div>

	<!-- fin modification modal -->

	<!-- debut de modale voir offre -->
	<div class="col-md-4 col-sm-12">
		<div class="modal fade bs-example-modal-lg" id="voirOffreModale" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title text-blue" id="myLargeModalLabel">Consulter une offre</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<div class="clearfix">
								<div class="pull-left">
									
								</div>
							</div>
						<div class="modal-body">
							<h5 >Intitulé de l'offre</h5>
							<p id="titleVoirModal" ></p>
							<h5  >Date d'ajout</h5>
							<p id="dateVoirModal"></p>
							<h5>Type de contrat</h5>
							<p id="contractVoirModal"></p>
							<h5>Description</h5>
							<p id= "descriptionVoirModal"></p>
							<h5>Compétences</h5>
							<ol id="skillVoirModal"> </ol>
						</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fin de modale voir offre -->

	<!-- Début de modale confirmation archive -->
	<div class="col-md-4 col-sm-12">
		<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
			<div class="modal fade" id="conf" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-body text-center font-18">
							<h4 class="padding-top-30 mb-30 weight-500">Etes-vous sûr de vouloir archiver ?</h4>
							<div class="padding-bottom-30 row" style="max-width: 170px; margin: 0 auto;">
								<div class="col-6">
									<button type="button" class="btn btn-secondary border-radius-100 btn-block confirmation-btn" data-dismiss="modal"><i class="fa fa-times"></i></button>
									NON
								</div>
								<div class="col-6">
									<button id="okArchive" type="button" class="btn btn-primary border-radius-100 btn-block confirmation-btn" data-dismiss="modal"><i class="fa fa-check"></i></button>
									OUI
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Fin de modale confirmation archive -->

	<?php include('include/footer.php'); ?>
		
	<?php include('include/script.php'); ?>
	<script src="/vendors/scripts/offresEmploi.js"></script>

</body>
</html>