<!DOCTYPE html>
<html lang="fr">
<html>

<head>
	<?php include('include/head.php'); ?>
</head>

<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h2>Liste des Skills</h2>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Skills</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<!-- Début de la table-->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<!-- <div class="clearfix mb-20">
						<div class="pull-left">
							<h3 class="font-23bdc3">Liste des Skills</h3>
						</div>
					</div> -->
					<div class="row">
						<table id="tableSkill"class="data-table stripe hover nowrap">
							<thead>
								<tr>
									<th>Designation</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($skills as $s): ?>
								<tr>
									<td class="table-plus"><?= $s->getDesignation()  ?></td>
									<td> <textarea class="textAreaDisable" disabled cols="40" rows="4"> <?= $s->getDescription() ?> </textarea> </td>
									<!-- <td>
										<div class="dropdown">
											<a class="btn btn-outline-primary dropdown-toggle" role="button" data-toggle="dropdown">
												<i class="fa fa-ellipsis-h"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" onclick = "modiOffreModale(event, php $e->getId() php )"><i class="fa fa-pencil modifier-modale"></i> Modifier</a>
												<a class="dropdown-item"  onclick = "archivage(event, <?= $s->getId() ?> )"><i class="fa fa-archive"></i> Archiver</a>
											</div>
										</div>
									</td> -->
								</tr>
								<?php endforeach ;?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Fin de la table  -->
			<!-- Fermeture "min-height-200" -->
			</div>
			<!-- Fermeture customscroll -->
		</div>
		<!-- Fermeture "main container" -->
	</div>
			<!-- Large modal -->
	<div class="col-md-4 col-sm-12">
		<div class="modal fade bs-example-modal-lg" id="creationSkillModale" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myLargeModalLabel">Ajouter un Skill</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">

						<!-- Default Basic Forms Start -->
						<!-- <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<div class="clearfix">
								<div class="pull-left">
									<h4 class="text-blue">Ajouter un Skill</h4>
								</div>
							</div> -->
							<form id=createFormSkill>
								<div class="form-group row">
									<label class="col-sm-12 col-md-2 col-form-label">Titre</label>
									<div class="col-sm-12 col-md-10">
										<input name="designation" id="designationCreateModal"class="form-control" type="text" placeholder="Intitulé du Skill">
									</div>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="description" id="descriptionCreateModalSkill" placeholder="une courte description du Skill" class="form-control"></textarea>
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
						<button id="btnCreateSkill" type="button" class="btn btn-primary">Créer</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Début de modale confirmation creation de skills -->
	<div class="col-md-4 col-sm-12">
		<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
			<div class="modal fade" id="conf" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-body text-center font-18">
							<h4 class="padding-top-30 mb-30 weight-500">Etes-vous sûr de vouloir archiver ?</h4>
							<div class="padding-bottom-30 row" style="max-width: 170px; margin: 0 auto;">
								<div class="col-6">
									<button type="button" class="btn btn-secondary border-radius-100 btn-block confirmation-btn" data-dismiss="modal"><i class="fa fa-times"></i></button>
									NON
								</div>
								<div class="col-6">
									<button id="okArchive" type="button" class="btn btn-primary border-radius-100 btn-block confirmation-btn" data-dismiss="modal"><i class="fa fa-check"></i></button>
									OUI
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Fin de modale confirmation archive -->

	<?php include('include/footer.php'); ?>
		
	<?php include('include/script.php'); ?>
	<script src="/vendors/scripts/skill.js"></script>

</body>
</html>