<!DOCTYPE html>
<html>
<head>
	<?php include('include/head.php'); ?>

</head>
<body>

	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>

    <div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Vos offres d'emploi archivées</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/recherche_stagiaire">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Archives offres</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<!-- Début de la table-->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<div class="clearfix mb-20">
						<div class="pull-left">
							<h5 class="text-blue">Liste des offres</h5>
						</div>
					</div>
					<div class="row">
						<table id="tableArchive" class="data-table stripe hover nowrap">
							<thead>
								<tr>
									<th class="table-plus datatable-nosort">Date</th>
									<th>Intitulé de l'offre</th>
									<th>Type de contrat</th>
									<th>Description</th>
									<th>Compétences requises</th>
									<th class="datatable-nosort">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								
								<?php foreach ($offres as $key => $value):?>
									<td class="table-plus" id = "dateCreation"><?= $value->getDate() ?></td>
									<td id =  "titreOffre"><?=  $value->getTitle() ?></td>
									<td id = "contrat"><?= $value->getContract()->getTitle() ?></td> 
									<td id = "description"><textarea readonly rows= "4" cols = "60"> <?= $value->getDescription() ?></textarea></td>
									<td id = "competences">
										<ul>
											<?php foreach ($value->getSkills() as $s): ?>

												<li><?= $s->getDesignation() ?></li>
											
											<?php endforeach ;?>
										</ul>
									</td>
									<td>
										<div class="dropdown">
											<a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
												<i class="fa fa-ellipsis-h"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#" onclick ="voirOffreArchivee(event, <?= $value->getId() ?>)"><i class="fa fa-eye voir-modale"></i> Voir</a>
												<!-- ci-dessous, je passe l'id dans lafonction creationOffre. Je récupère  l'id par php, et il sera utilisé dans la fonction en js -->
												<!-- <a class="dropdown-item" href="#" onclick = "creationOffreParId(event, php//$value->getId() php)"><i class="fa fa-pencil modifier-modale"></i> Créer nouvelle</a> -->
											</div>
										</div>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Fin de la table  -->
			<!-- Fermeture "min-height-200" -->
			</div>
		<!-- Fermeture customscroll -->
		</div>
	<!-- Fermeture "main container" -->
	</div>

	<!-- Ajouter  une nouvelle offre  -->
	<div class="col-md-4 col-sm-12">
		<div class="modal fade bs-example-modal-lg" id="nouvelleOffreModale" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myLargeModalLabel">Ajouter une offre</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<!-- Default Basic Forms Start -->
						<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<div class="clearfix">
								<div class="pull-left">
									<h4 class="text-blue">Ajouter une offre</h4>
									<p></p>
								</div>
							</div>
							<div class="modal-body">
								<form method = "POST" id = "formulairePost">
									<div class="form-group row">
										<label class="col-sm-12 col-md-2 col-form-label">Date *</label>
										<div class="col-sm-12 col-md-10">
											<input required="required" id = "dateN" name = "date" class="form-control date-picker" placeholder="Date de création" type="text">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-12 col-md-2 col-form-label">Offre *</label>
										<div class="col-sm-12 col-md-10">
											<input required="required" id = "intituleN" name ="advertisement_title" class="form-control" type="text" placeholder="Intitulé de l'offre">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-12 col-md-2 col-form-label">Type de contrat *</label>
										<div class="col-sm-12 col-md-10">
											<select required="required" id = "contratN" name ="contract_title" class="custom-select col-12">
											<?php
												foreach($contracts as $c){
													echo "<option value='".$c->getId()."'>".$c->getTitle()."</option>";
											} ?>	
											</select>
										</div>
									</div>
									<div class="form-group">
										<label>Description *</label>
										<textarea required="required" id = "descriptionN" name = "description" class="form-control"></textarea>
									</div>
									<div class="form-group row">
										<label class="col-sm-12 col-md-2 col-form-label">Compétences requises *</label>
										<div class="col-sm-12 col-md-10">
											<select required="required" id = "skillsN" name = "designation[]" multiple class="custom-select form-control" >
											<!-- avec les [] après le name, php traite les valeurs comme un tableau -->										
											</select>
										</div>
									</div>
									<div class="modal-footer">
										<div>
											<p>* Champs obligatoires</p>
										</div>
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal" id = "btnCreateOffre">Envoyer</button>
									</div>
									<input style="display:none" class="form-control" type="text" name = "emitter" id = "emitterN">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- Fin de modale ajouter une nouvelle offre  -->
	</div>

 	<!-- debut de modale voir offre archivée -->
	<div class="col-md-4 col-sm-12">
		<div class="modal fade bs-example-modal-lg" id="voirArchiveModale" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myLargeModalLabel">Consulter une offre archivée</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<div class="clearfix">
								<div class="pull-left">
									<h4 class="text-blue">Consulter une offre archivée</h4>
									<p></p>
								</div>
							</div>
						<div class="modal-body">
							<h5>Date d'ajout</h5>
							<p id = "date-consult"></p>
							<h5 >Intitulé de l'offre</h5>
							<p id = "title-consult"></p>
							<h5>Type de contrat</h5>
							<p id ="contract-consult"></p>
							<h5>Description</h5>
							<p id = "description-consult"></p>
							<h5>Compétences</h5>
							<p id = "skill-consult"></p>
						</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fin de modale voir offre archivée -->
	
	<!-- debut de modale créer offre via ancienne-->
	<div class="col-md-4 col-sm-12">
		<div class="modal fade bs-example-modal-lg" id="modOffreModale" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myLargeModalLabel">Créer une offre</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<div class="clearfix">
								<div class="pull-left">
									<h4 class="text-blue">Créer une offre : </h4>
								
									<p></p>
								</div>
							</div>
							<div class="modal-body">
								<form id = "formulairePostArchive" action="/api/archives-emploi" method = "POST">
									<div class="form-group row">
										<label class="col-sm-12 col-md-2 col-form-label">Date*</label>
										<div class="col-sm-12 col-md-10">
											<input required="required"  class="form-control date-picker" name = "date" id = "date-modif">
										</div>
										</div>
									<div class="form-group row">
										<label class="col-sm-12 col-md-2 col-form-label">Offre*</label>
										<div class="col-sm-12 col-md-10">
											<input required="required"  class="form-control" type="text" name = "title" id = "title-modif">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-12 col-md-2 col-form-label">Type de contrat*</label>
										<div class="col-sm-12 col-md-10">
											<select required="required" class="custom-select col-12" name = "contract" id = "contract-modif">
												<?php
													foreach($contracts as $c){
														echo "<option value='".$c->getId()."'>".$c->getTitle()."</option>";
												} ?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label>Description</label>
										<textarea required="required"  class="form-control" name = "description" type="text" id = "description-modif"></textarea>
									</div>
									<div class="form-group row">
										<label class="col-sm-12 col-md-2 col-form-label">Compétences*</label>
										<div class="col-sm-12 col-md-10">
											<select required="required"  id = "skill-modif" name = "designation[]" multiple class="custom-select form-control" >
											<!-- avec les [] après le name, php traite les valeurs comme un tableau -->
												<?php
													foreach($skills as $s){
														echo "<option value='".$s->getId()."'>".$s->getDesignation()."</option>";
												} ?>
											</select>
										</div>
										<div class="modal-footer">
											<div>
												<p>* Champs obligatoires</p>
											</div>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
											<button type = "button" class="btn btn-primary" data-dismiss="modal" onclick = "postViaArchive()">Envoyer</button>
										</div>
										<input style="display:none" class="form-control" type="text" name = "emitter" id = "emitter-modif">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fin de modale modifier offre -->

	<?php include('include/footer.php'); ?>
	<?php include('include/script.php'); ?>
	<script src="/vendors/scripts/archivesEmploi.js"></script>
</body>
</html>

