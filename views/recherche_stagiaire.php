<!DOCTYPE html>
<html>
<head>
	<?php include('include/head.php'); ?>
	<link rel="stylesheet" type="text/css" href="src/plugins/datatables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="src/plugins/datatables/media/css/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="src/plugins/datatables/media/css/responsive.dataTables.css">
</head>
<body>
<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Recherche stagiaire</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="/recherche_stagiaire">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Recherche</li>
								</ol>
							</nav>
						</div>
						<div class="col-md-6 col-sm-12 text-right">
							<div class="dropdown">
								<a class="btn btn-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
									Option
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Exporter la Liste</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Datatable Simple debut -->
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<div class="clearfix mb-20">
						<div class="pull-left">
							<h3 class="text-blue">Portail de recherche stagiaire</h3>
						</div>
					</div>
					<div class="row">
						<table id="tableStagiaire" class="data-table stripe hover nowrap">
							<thead>
								<tr>
									<th class="table-plus datatable-nosort">Competence</th>
									<th>Nom</th>
									<th>Prenom</th>
									<th>Date de Naissance</th>
									<th>Adresse</th>
									<th class="datatable-nosort">Action</th>
								</tr>
							</thead>
							<tbody>
							
								<!-- Je mets une boucle dans le html pour pouvoir afficher tout les stagiaires sur ma vue -->
							    <?php foreach($recherche_stagiaire as $stagiaire): ?>
								    <tr>
										<td class="table-plus">
										<!-- boucle sur les stagiaires, à l'arrtibut abilities  -->
											<?php foreach($stagiaire->getAbilities() as $s) {?>	
											<!-- Maintenant, il est possible de récupérer les designations -->
											<p><?= $s->getDesignation() ?></p>
											<!-- Cette boucle se ferme avec les accolades car c'est une boucle dans une boucle, et il n'est pas possible de 
											fermer avec endforeach -->
											<?php } ?>
										</td>
										<td><?= $stagiaire->getLastName() ?></td>
										<td><?= $stagiaire->getFirstName() ?></td>
										<td><?= $stagiaire->getBirthday() ?></td>
										<td><?= $stagiaire->getEmail() ?></td>
										<td><a class="dropdown-item" data-toggle="modal" data-target="#email" href="#"><i class="ion-ios-email fa-2x"></i></a></td>
								    </tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Datatable Simple fin -->
			</div>


				<!-- Modal Email debut -->
					<div class="col-md-4 col-sm-12">
							<div class="modal fade bs-example-modal-lg" id="email" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="email">Email</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">
                                        <form>
						                    <div class="form-group row">
							                    <label class="col-sm-12 col-md-2 col-form-label">Email</label>
							                        <div class="col-sm-12 col-md-10">
								                        <input id="mail" class="form-control" placeholder="Merci d'indiquer l'adresse Email" type="email" >
							                        </div>
						                    </div>
											<div class="form-group row">
							                    <label class="col-sm-12 col-md-2 col-form-label">Objet</label>
							                        <div class="col-sm-12 col-md-10">
								                        <input class="form-control" placeholder="Objet" type="text" >
							                        </div>
						                    </div>
                                            <div class="form-group row">
							                    <label class="col-sm-12 col-md-2 col-form-label">Message</label>
							                        <div class="col-sm-12 col-md-10">
								                        <textarea class="form-control"></textarea>
							                        </div>
						                    </div>
					                    </form>
										</div>
										<div class="modal-footer">
											<a class="btn" data-dismiss="modal">Fermer</a>
											<a class="btn btn-primary" data-toggle="modal" data-target="#confirmation" data-dismiss="modal" href="#">Envoyer</a>
										</div>
									</div>
								</div>
							</div>
					</div>
					<!-- Modal Email fin -->

					<!-- Success Modal debut -->
					<div class="col-md-4 col-sm-12">
							<div class="modal fade" id="confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-body text-center font-18">
											<h3 class="mb-20" id="confirmation">Form Submitted!</h3>
											<div class="mb-30 text-center"><img src="vendors/images/success.png"></div>
											Votre mail a bien été envoyé
										</div>
										<div class="modal-footer justify-content-center">
											<button type="button" class="btn btn-primary" data-dismiss="modal" data-toggle="#email">Ok</button>
										</div>
									</div>
								</div>
							</div>
					</div>
					<!-- Success Modal fin -->
			<?php include('include/footer.php'); ?>
		</div>
	</div>
	<?php include('include/script.php'); ?>
	<script src="/vendors/scripts/recherche_stagiaire.js"></script>
</body>
</html>